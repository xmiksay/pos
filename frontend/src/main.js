import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { useWebSocket } from '@vueuse/core'
import App from './App.vue'
import router from './router'

import { useMainStore } from '@/stores/mainStore'
import { useConfigStore } from '@/stores/configStore'
import { useAuthStore } from '@/stores/authStore'
import { useDefinitionStore } from '@/stores/definitionStore'
import { useTableStore } from '@/stores/tableStore'
import { useInvoiceStore } from '@/stores/invoiceStore'
import { useSourceStore } from '@/stores/sourceStore'
import VueDatePicker from '@vuepic/vue-datepicker'
import '@vuepic/vue-datepicker/dist/main.css'

import 'vue-final-modal/style.css'

const schema = window.location.protocol.replace('http', 'ws')
const hostname = window.location.host

const app = createApp(App)
const pinia = createPinia()
const websocket = useWebSocket(`${schema}//${hostname}/api/barman`, {
  autoReconnect: true,
  retries: 3,
  delay: 1000,
  onFailed() {
    alert('Failed to connect WebSocket after 3 retries')
  }
})

app.provide('$websocket', websocket)
app.use(pinia)
app.use(router)

useMainStore()
useConfigStore()
useAuthStore()
useDefinitionStore()
useTableStore()
useInvoiceStore()
useSourceStore()

app.component('VueDatePicker', VueDatePicker)
app.mount('#app')
