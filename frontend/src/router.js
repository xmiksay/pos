import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'tables',
      component: () => import('./views/TableListView.vue')
    },
    {
      path: '/table/:uuid',
      name: 'table',
      component: () => import('./views/TableView.vue')
    },
    {
      path: '/invoices/',
      name: 'invoices',
      component: () => import('./views/InvoiceListView.vue')
    },
    {
      path: '/invoice/:uuid',
      name: 'invoice',
      component: () => import('./views/InvoiceView.vue')
    },
    {
      path: '/sources/',
      name: 'sources',
      component: () => import('./views/SourceListView.vue')
    },
    {
      path: '/source/:uuid',
      name: 'source',
      component: () => import('./views/SourceView.vue')
    },
    {
      path: '/report/',
      name: 'report',
      component: () => import('./views/ReportView.vue')
    },
    {
      path: '/definition',
      name: 'definition',
      component: () => import('./views/DefinitionView.vue')
    },
    {
      path: '/config',
      name: 'config',
      component: () => import('./views/ConfigView.vue')
    },
    {
      path: '/auth',
      name: 'auth',
      component: () => import('./views/AuthView.vue')
    }
  ]
})

export default router
