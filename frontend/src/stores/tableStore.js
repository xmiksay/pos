import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useMainStore } from '@/stores/mainStore'
import { useDefinitionStore } from '@/stores/definitionStore'
import { v4 as uuidv4 } from 'uuid'
import { sort, deepCopy } from '@/utils'

export const useTableStore = defineStore('table', () => {
  const mainStore = useMainStore()
  const definitionStore = useDefinitionStore()
  const data = ref({})
  const oldData = ref({})

  const tables = computed(() => {
    if (data.value) {
      return sort(Object.values(data.value), 'order')
    }

    return []
  })

  mainStore.on('tables', (value) => {
    data.value = value
    oldData.value = deepCopy(value)
  })

  mainStore.on('table', (value) => {
    data.value[value.uuid] = value
    oldData.value[value.uuid] = deepCopy(value)
  })

  function set() {
    mainStore.send('tableSet', data.value)
  }

  function sync(table) {
    mainStore.send('tableSync', table)
  }

  function reset() {
    console.log('Reset table store')
    data.value = deepCopy(oldData.value)
  }

  function tableCreate(name, roomUuid, order = 1000) {
    const uuid = uuidv4()
    const table = {
      uuid,
      name,
      order,
      roomUuid
    }
    sync(table)
  }

  function tableRemove(uuid) {
    console.log('Remove room', uuid)
    delete data.value[uuid]
    set()
  }

  function tableName(uuid, other = 'undefined') {
    return data.value[uuid]?.name || other
  }

  function tableRoomName(uuid, other = 'undefined') {
    return definitionStore.roomName(data.value[uuid]?.roomUuid, other) || other
  }

  function print(uuid) {
    mainStore.send('printTable', uuid)
  }

  return {
    data,
    tables,
    set,
    sync,
    reset,
    print,
    tableCreate,
    tableRemove,
    tableName,
    tableRoomName
  }
})
