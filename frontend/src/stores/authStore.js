import { ref, computed } from 'vue'
import { deepCopy } from '@/utils'
import { defineStore } from 'pinia'
import { useMainStore } from './mainStore'

export const useAuthStore = defineStore('auth', () => {
  const mainStore = useMainStore()
  const init = {
    users: {},
    tokens: {},
    devices: {}
  }
  const data = ref(init)
  const oldData = ref(init)

  mainStore.on('auth', (value) => {
    // console.log("On definition: ", value)

    data.value = value
    oldData.value = deepCopy(data.value)
  })

  const users = computed(() => {
    if (mainStore.isLogged && data.value?.users) {
      return Object.values(data.value?.users) || []
    } else {
      return []
    }
  })
  const tokens = computed(() => Object.values(data.value?.tokens) || [])

  const devices = computed(() => Object.values(data.value?.devices) || [])

  function getName(uuid) {
    return (
      data.value?.users[uuid]?.name ||
      (mainStore.userInfo?.uuid == uuid ? mainStore.userInfo?.nick : undefined) ||
      'Unknown'
    )
  }

  return { mainStore, data, users, tokens, devices, getName }
})
