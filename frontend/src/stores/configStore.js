import { ref } from 'vue'
import { defineStore } from 'pinia'
import { deepCopy } from '@/utils'
import { useMainStore } from './mainStore'

export const useConfigStore = defineStore('config', () => {
  const mainStore = useMainStore()

  const init = {
    base: {},
    setting: {}
  }
  const data = ref(init)
  const oldData = ref(init)

  mainStore.on('config', (value) => {
    data.value = value
    oldData.value = deepCopy(value)
  })

  mainStore.on('setting', (value) => {
    if (data.value?.setting) {
      data.value.setting = value
      oldData.value.setting = deepCopy(value)
    } else {
      data.value = { setting: value }
      oldData.value = { setting: deepCopy(value) }
    }
  })

  function set() {
    mainStore.send('configSet', data.value)
  }

  return { mainStore, data, set }
})
