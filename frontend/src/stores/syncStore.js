import { defineStore } from 'pinia'
import { useMainStore } from './mainStore'
import { useDefinitionStore } from './definitionStore'
import { useTableStore } from './tableStore'
import { useInvoiceStore } from './invoiceStore'
import { v4 as uuidv4 } from 'uuid'
import { PAYMENT, PAYMENT_NUMBER } from '@/definitions'

export const useSyncStore = defineStore('sync', () => {
  const mainStore = useMainStore()
  const tableStore = useTableStore()
  const invoiceStore = useInvoiceStore()
  const definitionStore = useDefinitionStore()

  function newTableItem(productUuid) {
    return {
      productUuid,
      quantity: 0
    }
  }

  function newInvoice(tableUuid) {
    return {
      uuid: uuidv4(),
      date: new Date(),
      isCanceled: false,
      isLocked: false,
      items: [],
      paidAmount: 0,
      paidDate: null,
      sequence: null,
      tableUuid,
      userUuid: String(mainStore.userInfo.uuid)
    }
  }

  function newInvoiceItem(productUuid, tableItemUuid) {
    return {
      name: definitionStore.productName(productUuid),
      price: definitionStore.productPrice(productUuid),
      productUuid,
      tableItemUuid,
      quantity: 0
    }
  }

  function getTableInvoiceItems(tableUuid, productUuid) {
    const table = tableStore.data[tableUuid]
    const tableItem = table.items?.find((i) => i.productUuid == productUuid)
    // Stop, if table item does not exists
    if (tableItem === undefined || tableItem === null) throw new Error('TableItem does not exist')

    let invoice = invoiceStore.data[table.invoiceUuid]

    // Ensure that invoice exists
    if (invoice === undefined) {
      invoice = newInvoice(table.uuid)
      table.invoiceUuid = invoice.uuid
      invoiceStore.data[invoice.uuid] = invoice
    }

    let invoiceItem = invoice.items?.find((i) => i.productUuid == productUuid)
    if (invoiceItem === undefined || invoiceItem === null) {
      invoiceItem = newInvoiceItem(productUuid, tableItem.uuid)
      invoice.items.push(invoiceItem)
    }

    return [table, tableItem, invoiceItem]
  }

  function getInvoiceTableItems(invoiceUuid, productUuid) {
    const invoice = invoiceStore.data[invoiceUuid]
    const invoiceItem = invoice.items?.find((i) => i.productUuid == productUuid)
    if (invoiceItem === null || invoiceItem === undefined)
      throw new Error('InvoiceItem does not exist')

    const table = tableStore.data[invoice.tableUuid]

    // Ensure that invoice exists
    if (table === undefined) {
      throw new Error('Table does not exist')
    }

    let tableItem = table.items?.find((i) => i.productUuid == productUuid)
    if (tableItem === undefined || tableItem === null) {
      tableItem = newTableItem(productUuid)
      table.items.push(tableItem)
    }

    return [invoice, invoiceItem, tableItem]
  }

  function itemAdd(store, callback, uuid, productUuid) {
    const obj = store.data[uuid]
    if (obj.items === null) {
      obj.items = []
    }

    let item = obj.items.find((i) => i.productUuid == productUuid)
    if (item === undefined) {
      item = callback(productUuid)
      obj.items.push(item)
    }

    item.quantity++
  }

  function itemDel(store, uuid, productUuid) {
    const obj = store.data[uuid]
    if (obj.items === null) {
      obj.items = []
    }

    let item = obj.items.find((i) => i.productUuid == productUuid)
    if (item !== undefined) {
      if (item.quantity > 1) {
        item.quantity--
      } else {
        obj.items.splice(obj.items.indexOf(item), 1)
      }
    }
  }

  function tableItemAdd(tableUuid, productUuid) {
    itemAdd(tableStore, newTableItem, tableUuid, productUuid)
  }

  function tableItemDel(tableUuid, productUuid) {
    itemDel(tableStore, tableUuid, productUuid)
  }

  function tableToInvoiceOne(tableUuid, productUuid) {
    const [table, tableItem, invoiceItem] = getTableInvoiceItems(tableUuid, productUuid)

    // Decrease or remove
    if (tableItem.quantity > 1) {
      tableItem.quantity--
    } else {
      table.items.splice(table.items.indexOf(tableItem), 1)
    }

    // increase invoice item
    invoiceItem.quantity++
  }

  function tableToInvoiceAll(tableUuid, productUuid) {
    const [table, tableItem, invoiceItem] = getTableInvoiceItems(tableUuid, productUuid)

    // Decrease or remove
    const quantity = tableItem.quantity
    table.items.splice(table.items.indexOf(tableItem), 1)
    // increase invoice item
    invoiceItem.quantity += quantity
  }

  function tableToInvoice(tableUuid) {
    const table = tableStore.data[tableUuid]
    const items = table.items.map((i) => i.productUuid)
    for (const item of items) {
      tableToInvoiceAll(table.uuid, item)
    }
  }

  function invoiceItemAdd(invoiceUuid, productUuid) {
    itemAdd(invoiceStore, newInvoiceItem, invoiceUuid, productUuid)
  }

  function invoiceItemDel(invoiceUuid, productUuid) {
    itemDel(invoiceStore, invoiceUuid, productUuid)
  }

  function invoiceToTableOne(invoiceUuid, productUuid) {
    const [invoice, invoiceItem, tableItem] = getInvoiceTableItems(invoiceUuid, productUuid)

    // Decrease or remove
    if (invoiceItem.quantity > 1) {
      invoiceItem.quantity--
    } else {
      invoice.items.splice(invoice.items.indexOf(invoiceItem), 1)
    }

    // increase invoice item
    tableItem.quantity++
  }

  function invoiceToTableAll(invoiceUuid, productUuid) {
    const [invoice, invoiceItem, tableItem] = getInvoiceTableItems(invoiceUuid, productUuid)
    // Decrease or remove
    const quantity = invoiceItem.quantity
    invoice.items.splice(invoice.items.indexOf(invoiceItem), 1)
    // increase invoice item
    tableItem.quantity += quantity
  }

  function tableToTableOne(table1, table2, productUuid) {
    const item1 = table1.items.find((i) => i.productUuid == productUuid)
    if (item1 === undefined || item1 === null) throw new Error('TableItem does not exist')

    if (item1.quantity > 2) {
      item1.quantity -= 1
    } else {
      table1.items.splice(table1.items.indexOf(item1), 1)
    }

    let item2 = table2.items.find((i) => i.productUuid == productUuid)
    if (item2 === undefined || item2 === null) {
      item2 = newTableItem(productUuid)
      table2.items.push(item2)
    }
    item2.quantity += 1
  }

  function tableToTableAll(table1, table2, productUuid) {
    const item1 = table1.items.find((i) => i.productUuid == productUuid)
    if (item1 === undefined || item1 === null) throw new Error('TableItem does not exist')

    const quantity = item1.quantity
    table1.items.splice(table1.items.indexOf(item1), 1)

    if (table2.items === undefined || table2.items === null) {
      table2.items = []
    }

    let item2 = table2.items.find((i) => i.productUuid == productUuid)
    if (item2 === undefined || item2 === null) {
      item2 = newTableItem(productUuid)
      table2.items.push(item2)
    }
    item2.quantity += quantity
  }

  function tableToTable(table1, table2) {
    const items = table1.items.map((i) => i.productUuid)
    for (let productUuid of items) {
      tableToTableAll(table1, table2, productUuid)
    }
  }

  function randomSequence() {
    const MAX = 5
    return Math.floor(Math.random() * MAX) % MAX == 0
  }

  function pad(num, size) {
    num = num.toString()
    while (num.length < size) num = '0' + num
    return num
  }

  function getSequence(payment) {
    const now = new Date()
    const start = new Date(now.getFullYear(), now.getMonth(), 1)
    const number =
      invoiceStore.invoices.filter(
        (i) => i.sequence !== null && i.payment == payment && new Date(i.paidDate) > start
      ).length + 1

    // Y - year
    // M - month
    // P - payment
    // N - number
    // YYYYMM0P0NNNN
    // 2024060900001
    return `${pad(start.getFullYear(), 4)}${pad(start.getMonth() + 1, 2)}0${PAYMENT_NUMBER[payment]}0${pad(number, 4)}`
  }

  function pay(invoice, amount = 0) {
    invoice.paidDate = new Date()
    invoice.paidAmount = amount
    invoice.isLocked = true
    invoice.userUuid = mainStore.userInfo?.uuid || invoice.userUuid

    const table = tableStore.data[invoice.tableUuid]
    if (table) {
      table.invoiceUuid = null
      tableStore.sync(table)
    }
    invoiceStore.sync(invoice)
    invoiceStore.print(invoice.uuid)
  }

  function payCash(invoice, amount = 0, sequence = false) {
    invoice.payment = PAYMENT.CASH

    if (!invoice.sequence && (sequence || randomSequence())) {
      invoice.sequence = getSequence(PAYMENT.CASH)
    }
    pay(invoice, amount)
  }

  function payCard(invoice, amount = 0) {
    invoice.payment = PAYMENT.CARD
    invoice.sequence = getSequence(PAYMENT.CARD)
    pay(invoice, amount)
  }

  function print(printerUuid, data) {
    mainStore.send('print', { printer: printerUuid, data })
  }

  return {
    tableItemAdd,
    tableItemDel,
    tableToInvoiceOne,
    tableToInvoiceAll,
    tableToInvoice,

    invoiceItemAdd,
    invoiceItemDel,
    invoiceToTableOne,
    invoiceToTableAll,

    tableToTableOne,
    tableToTableAll,
    tableToTable,

    pay,
    payCash,
    payCard,

    print
  }
})
