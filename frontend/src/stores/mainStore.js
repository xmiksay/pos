import { ref, computed, inject, watch } from 'vue'
import { defineStore } from 'pinia'

export const useMainStore = defineStore('main', () => {
  const websocket = inject('$websocket')
  const register = {}
  const userInfo = ref(null)
  const tokenActive = ref(null)
  const tokenStore = ref(
    (() => {
      try {
        const tokens = JSON.parse(localStorage.getItem('tokens')) || {}
        tokenActive.value = Object.values(tokens).find((t) => !t.locked)?.value
        console.log('Active token!', tokenActive.value)
        return tokens
      } catch (e) {
        console.error(e)
        return {}
      }
    })()
  )
  const lastUpdate = ref(new Date())
  const tokens = computed(() => Object.values(tokenStore.value).filter((t) => t?.locked === true))
  const weight = ref(null)

  const isLocked = computed(() => {
    return userInfo.value?.isLocked == true
  })
  const isAdmin = computed(() => {
    return userInfo.value?.isAdmin === true
  })

  const isLogged = computed(() => {
    return userInfo.value !== null
  })

  function on(tag, callback) {
    console.log('Register: ', tag)
    register[tag] = callback
  }

  function send(tag, content) {
    websocket.send(JSON.stringify({ tag, content }))
  }

  function login(nick, password) {
    send('login', { nick, password })
    console.log('LogIn', nick, password)
  }

  function token(value) {
    send('token', value)
  }

  function logout() {
    console.log('LogOut')
    tokenActive.value = null
    send('logout')
  }

  function tokenGet(locked = true) {
    send('tokenGet', locked)
  }

  function lock() {
    console.log('Lock screen')
    send('lock')
  }

  function unlock(pin) {
    send('unlock', pin)
    console.log('UnLock screen', pin)
  }

  function tokenSave() {
    localStorage.setItem('tokens', JSON.stringify(tokenStore.value))
  }

  on('userInfo', (value) => {
    console.log('On userInfo', value)
    userInfo.value = value

    // If there is active token, send it
    if (!userInfo.value && tokenActive.value) {
      token(tokenActive.value)
    }

    // If we logged succesfully, get token for auto relog
    if (userInfo.value) {
      if (!tokens.value.find((t) => t.nick == value?.nick)) {
        tokenGet(true)
      }
      if (!tokenActive.value) {
        tokenGet(false)
      }
    }
  })

  on('tokenFailed', (value) => {
    console.log('Removing token')
    if (value == tokenActive.value) {
      tokenActive.value = null
    }

    if (value in tokenStore.value) {
      delete tokenStore.value[value]
      tokenSave()
    }
  })

  on('tokenInfo', (tokenInfo) => {
    console.log(tokenInfo)
    if (tokenInfo.locked === false) {
      tokenActive.value = tokenInfo.value
    } else {
      tokenStore.value[tokenInfo.value] = tokenInfo
      tokenSave()
    }
  })

  on('weight', (value) => {
    weight.value = value
  })

  watch(
    websocket.data,
    (val) => {
      const msg = JSON.parse(val)
      lastUpdate.value = new Date()
      console.log("data change", lastUpdate.value)
      if (msg.tag in register) {
        register[msg.tag](msg.content)
      }
    },
    { lazy: true }
  )

  /*
    watch(websocket.status, val => {
        console.log(val)
        if (val == 'OPEN') {
            login('admin', 'admin')
        }
    })
    */

  return {
    lastUpdate,
    userInfo,
    tokens,
    isLogged,
    isLocked,
    isAdmin,
    weight,
    on,
    send,
    login,
    token,
    logout,
    lock,
    unlock,
    tokenGet
  }
})
