import { ref, computed } from 'vue'
import { v4 as uuidv4 } from 'uuid'
import { defineStore } from 'pinia'
import { sort, deepCopy } from '@/utils'
import { useMainStore } from './mainStore'

export const useSourceStore = defineStore('source', () => {
  const mainStore = useMainStore()

  const data = ref({})
  const oldData = ref({})

  const sources = computed(() => sort(Object.values(data.value || {}), 'date').reverse())

  mainStore.on('sources', (value) => {
    data.value = value
    oldData.value = deepCopy(value)
  })

  mainStore.on('source', (value) => {
    data.value[value.uuid] = value
    oldData.value[value.uuid] = deepCopy(value)
  })

  function sync(source) {
    console.log(source)
    mainStore.send('sourceSync', source)
  }

  function reset() {
    console.log('Reset source store')
    data.value = deepCopy(oldData.value)
  }

  function create() {
    const source = {
      uuid: uuidv4(),
      userUuid: mainStore.userInfo.uuid,
      date: new Date()
    }
    data.value[source.uuid] = source
    sync(source)
    return source
  }

  function remove(uuid) {
    const source = data.value[uuid]
    if (source) {
      source.deleted = true
      delete data.value[uuid]
      sync(source)
    }
  }

  return { mainStore, data, sources, sync, reset, create, remove }
})

export function sumItems(source) {
  return (
    source?.items?.reduce((acc, item) => acc + Number(item.quantity) * Number(item.price), 0) || 0
  )
}
