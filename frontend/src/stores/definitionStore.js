import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { deepCopy } from '@/utils'
import { useMainStore } from './mainStore'
import { v4 as uuidv4 } from 'uuid'
import { UNIT } from '@/definitions'

export const useDefinitionStore = defineStore('definition', () => {
  const mainStore = useMainStore()
  const init = { products: {}, tags: {}, metas: {}, suppliers: {}, rooms: {}, things: {} }
  const data = ref(init)
  const oldData = ref(init)

  mainStore.on('definition', (value) => {
    // console.log("On definition: ", value)

    data.value = value
    oldData.value = deepCopy(data.value)
  })

  const tags = computed(() => {
    // console.log("Computed tags", data.value?.tags)
    return data.value?.tags || {}
  })

  const suppliers = computed(() => {
    return data.value?.suppliers || {}
  })

  const metas = computed(() => {
    // console.log("Computed metas", data.value?.metas)
    return data.value?.metas || []
  })

  const products = computed(() => {
    // console.log("Computed products", data.value?.products)
    return data.value?.products || {}
  })

  const things = computed(() => {
    // console.log("Computed products", data.value?.products)
    return data.value?.things || {}
  })

  const rooms = computed(() => {
    // console.log("Computed rooms", data.value?.rooms)
    return data.value?.rooms || {}
  })

  function set() {
    mainStore.send('definitionSet', data.value)
  }

  function reset() {
    console.log('Reset definition store')
    data.value = deepCopy(oldData.value)
  }

  function tagCreate(name) {
    const uuid = uuidv4()
    console.log('Create tag', uuid, name)
    data.value.tags[uuid] = { name, uuid }
    set()
  }

  function tagRemove(uuid) {
    console.log('Remove tag', uuid)
    delete data.value.tags[uuid]
    set()
  }

  function supplierCreate(name) {
    const uuid = uuidv4()
    data.value.suppliers[uuid] = { name, uuid, data: {} }
    set()
  }

  function supplierName(uuid) {
    return data.value.suppliers[uuid]?.name
  }

  function roomCreate(name) {
    const uuid = uuidv4()
    console.log('Create room', uuid, name)
    data.value.rooms[uuid] = { name, uuid }
    set()
  }

  function roomRemove(uuid) {
    console.log('Remove room', uuid)
    delete data.value.rooms[uuid]
    set()
  }

  function roomName(uuid, other = 'undefined') {
    return data.value.rooms[uuid]?.name || other
  }

  function metaCreate(name) {
    const uuid = uuidv4()
    console.log('Create meta', uuid, name)
    data.value.metas[uuid] = { name, uuid }
    set()
  }

  function metaRemove(uuid) {
    console.log('Remove room', uuid)
    delete data.value.metas[uuid]
    set()
  }

  function metaName(uuid, other) {
    return data.value.metas[uuid]?.name || other
  }

  function metaUnit(uuid) {
    return data.value.metas[uuid]?.unit || UNIT.PIECE
  }

  function productCreate(name) {
    const uuid = uuidv4()
    console.log('Create product', uuid, name)
    data.value.products[uuid] = { name, uuid }
    set()
  }

  function productRemove(uuid) {
    console.log('Remove product', uuid)
    delete data.value.products[uuid]
    set()
  }

  function productName(uuid, other, size) {
    const name = data.value.products[uuid]?.name || other
    if (size > 0 && name.length > size) {
      return name.substring(0, size) + '...'
    } else {
      return name
    }
  }

  function productPrice(uuid) {
    return Number(data.value.products[uuid]?.price) || 0
  }

  function productUnit(uuid) {
    const product = data.value.products[uuid]
    const meta = data.value.metas[product?.metaUuid]
    return meta?.unit
  }

  function thingCreate(name) {
    const uuid = uuidv4()
    data.value.things[uuid] = { name, uuid }
    set()
  }

  function thingtRemove(uuid) {
    console.log('Remove thing', uuid)
    delete data.value.things[uuid]
    set()
  }

  function thingName(uuid, other) {
    return data.value.things[uuid]?.name || other
  }

  function thingUnit(uuid) {
    const thing = data.value.things[uuid]
    const meta = data.value.metas[thing?.metaUuid]
    return meta?.unit
  }

  function tagListName(tags) {
    const names = tags.map((t) => data.value.tags[t]?.name)
    return names.join(',')
  }

  return {
    data,
    tags,
    suppliers,
    metas,
    products,
    things,
    rooms,
    set,
    reset,

    tagCreate,
    tagRemove,

    supplierCreate,
    supplierName,

    roomCreate,
    roomRemove,
    roomName,

    metaCreate,
    metaRemove,
    metaName,
    metaUnit,

    productCreate,
    productRemove,
    productName,
    productPrice,
    productUnit,

    thingCreate,
    thingtRemove,
    thingName,

    thingUnit,
    tagListName
  }
})
