import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { deepCopy } from '@/utils'
import { useMainStore } from './mainStore'
import { sort } from '@/utils'

export const useInvoiceStore = defineStore('invoice', () => {
  const mainStore = useMainStore()

  const data = ref({})
  const oldData = ref({})

  const invoices = computed(() => sort(Object.values(data.value), 'date').reverse())

  mainStore.on('invoices', (value) => {
    console.log(value)
    data.value = value
    oldData.value = deepCopy(value)
  })

  mainStore.on('invoice', (value) => {
    data.value[value.uuid] = value
    oldData.value[value.uuid] = deepCopy(value)
  })

  function sync(invoice) {
    mainStore.send('invoiceSync', invoice)
  }

  function reset() {
    console.log('Reset invoice store')
    data.value = deepCopy(oldData.value)
  }

  function print(uuid) {
    mainStore.send('printInvoice', uuid)
  }

  return {
    data,
    invoices,
    sync,
    reset,
    print
  }
})

export function sumItems(invoice) {
  return (
    invoice?.items?.reduce((acc, item) => acc + Number(item.quantity) * Number(item.price), 0) || 0
  )
}
