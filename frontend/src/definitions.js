export const UNIT = {
  PIECE: 'piece',
  VOLUME: 'volume',
  WEIGHT: 'weight'
}
export const units = [UNIT.PIECE, UNIT.VOLUME, UNIT.WEIGHT]
export const unitTable = {
  [UNIT.VOLUME]: [
    [1000, 'l'],
    [100, 'dl'],
    [10, 'cl'],
    [1, 'ml']
  ],
  [UNIT.WEIGHT]: [
    [1000000, 't'],
    [1000, 'kg'],
    [1, 'g']
  ],
  [UNIT.PIECE]: [
    [1000000, 'M  ks'],
    [1000, 'k ks'],
    [1, 'ks']
  ]
}

export function unitToString(amount, unit, decimal = 1) {
  const line = unitTable[unit]
  if (line) {
    for (let pair of line) {
      if (amount >= pair[0]) {
        return pair[0] == 1
          ? `${amount} ${pair[1]}`
          : `${(amount / pair[0]).toFixed(decimal)} ${pair[1]}`
      }
    }
  }
  if (amount) {
    return amount.toString()
  }
  return ''
}

export const PAYMENT = {
  CASH: 'cash',
  CARD: 'card',
  QRCODE: 'qrcode',
  OTHER: 'other'
}
export const PAYMENT_NUMBER = {
  [PAYMENT.CASH]: 9,
  [PAYMENT.CARD]: 8
}
export const payments = [PAYMENT.CASH, PAYMENT.QRCODE, PAYMENT.CARD, PAYMENT.OTHER]

export const ZERO_UUID = '00000000-0000-0000-0000-000000000000'

export const SOURCE_KIND = {
  NEW: 'new',
  PREPARE: 'prepare',
  ORDERED: 'ordered',
  DELIVERED: 'delivered',
  PAID: 'paid'
}
export const sourceKinds = [
  SOURCE_KIND.NEW,
  SOURCE_KIND.PREPARE,
  SOURCE_KIND.ORDERED,
  SOURCE_KIND.DELIVERED,
  SOURCE_KIND.PAID
]
