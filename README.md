#Run:

```
cargo build
cargo server
```

```
npm install --prefix frontend
npm run --prefix frontend dev
npm run --prefix frontend build
```

`docker run --rm -it -v ./data/:/app/data/ -p 3030:3030 registry.gitlab.com/xmiksay/pos ./server --config /app/data/server_config.json`