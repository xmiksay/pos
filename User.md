# Pokladna
Aplikace na spravu uctu a stolu v restaraci

## Stoly
Stoly jsou rozlisene barvickama dle legendy v paticce - Novy, Obsazeny, Dlouho neobslouzeny a vyvorit.
Dale jsou stoly rozdelene podle prostoru, kde se nachazi (hospoda, zahradka)

Pro vyber stolu se prejde na detail stolu.

Na detailu stolu lze na stul pridavat zbozi z prostredniho sloucce. Pokud je jiz vytvorna uctenka, lze pridavat polozky primo uctenku.
Lze vytisknout prehled stolu (informace o stolu, jednotlive polozky a suma - vhodne pro lidi, co nevi, kolik maji u sebe penez).
Po pridani na uctenku se zobrazi detail uctenky s akcema zaplatit a faktura.

V Menu zaplatit lze vybrat realna castka, ktera byla placena zakaznikem a moznosti platby. Taky lze vytisknout jen prehled polozek na fakture.

* zaplatit - platebni moznost cash s automatickym procentualni propisem na fakturu.
* hotove - podobne, jako menu zaplatit, ale na fakturu se propise vzdy
* karta - zvoli se moznost kartou
* tisk - pouze vytiskne fakturu


Vsechny volby zpusobi automaticke vytisknuti na tiskarne.

## Faktury
System na spravu faktur/uctenek, ktera hospoda vydava. Vetsina faktur vznika automaticky z polozek, ktere se v hospode prodavaji, ale pridat i rucne fakturu, ktera s hospodou nezouvisi (napr. nekdo rozbije a zaplati stul).
Do reportu se pocitaji pouze faktury, ktere maji nastavenou polozku Zaplaceno.

Zaroven jde z menu taky fakturu primo zaplatit

## Zdroje
System na spravu zdroju, coz by slo nazvat jako prijate faktury (od dodavatelu). Zdroje se vazou na uzivatele (idealne uzivatel, ktery zdroj zaplatit), tak dodavatele (kolik mesicne stoji dany dodavatel).
Zaroven ma zdroj stavy:

* new - novy zdroj, nepocita se do statistik
* prepare - priprava na objednavku - sepsani polozek, co je treba objednat u dodavatele
- ordered - polozky uz byly objednany
- paid - polozky uz byly zaplaceny


## Report
Souhrne statistiky a vypocty z fungovani pokladny. Administratori si muzou vylistovat jakykoli rozsah pro vsechny uzivatele,
bezny uzivatel vidi jen verzi za poslednich 12hodin (typicky kalkulace po smene)

* Platby - rozdeleni uctenek podle druhu platby - muze obsahovat platby, ktere nesouvisi se zbozim
* Obsluha - rozdeleni uctenek podle uzivatele, co obsluhoval - muze obsahovat platby, ktere nesouvisi se zbozim
* Prodano - Suma prodaneho zbozi v mernych jednotkach (13l piva)
* Product - Sumar jednotlivych polozek, co se prodalo
* Nakoupeno - Suma vsech prichozich faktur rozdelena podle polozek (Pivo kamenice 150l)
* Thing - Jednotlive polozky, co byly na fakture (Pivo kamenice 12 50l 3x)
* Dodavatel - Souhrne platby pro daneho dodavatele (Maneo 13 polozek 5000CZK)
* Zaplatil - souhrn plateb od daneho uzivatele (Jirka 10000 lavicky)

* vydano uctenek - Celkovy pocet vydanych uctenek
* zaplaceno - realnepenize, ktere zakaznicy zaplatili
* uctovano - penize, ktere byly zakaznikum uctovane
* Nakoupeno - penize, ktere byly zaplacene dodavatelum
* profit - rozdil mezi Zaplaceno - Nakoupeno
* Dyska - rozdil mezi zaplaceno - uctovano

## Definition
* Meta - Naproduct, ktery urcuje zbozi a jednotky. Napriklad Kamenice 12 - objem
* Produkt - Zbozi, ktere se prodava z daneho meta produktu. Napriklad. (Kamenice 12 0.5L)
* Thing - Zbozi, ktere se prijima pro dany meta produkt. Naprikl (Kamenice 12 50l)
* Tag - pomocna vec pro rychlejsi vyber zbozi. Meta, Zbozi a Thing jdou oznacit timto tagem pro snadnejsi vyber.
* Supplier - seznam dodavatelu
* Rooms - seznam mistnosti
* Tables - Seznam stolu