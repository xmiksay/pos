FROM rust:1.78-slim
WORKDIR /app
COPY out/ ./
VOLUME /app/data
CMD /app/x86_64/server