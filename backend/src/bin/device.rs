use clap::Parser;
use pos::db::LoadSave;
use tracing_subscriber::prelude::*;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    #[arg(short, long)]
    config: String,
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let fmt_layer = tracing_subscriber::fmt::layer().with_line_number(true);
    let filter = tracing_subscriber::filter::Targets::new()
        .with_target(env!("CARGO_PKG_NAME"), tracing::Level::TRACE)
        .with_target(env!("CARGO_BIN_NAME"), tracing::Level::TRACE);
    tracing_subscriber::registry()
        .with(fmt_layer)
        .with(filter)
        .init();

    tracing::info!(
        "Commit: {} Version: {}",
        env!("GIT_COMMIT"),
        env!("CARGO_PKG_VERSION")
    );

    tracing::debug!(
        "{}",
        serde_json::to_string(&pos::db::config::Device::default()).unwrap()
    );

    let args = Args::parse();

    let config = pos::db::config::Device::load(&args.config)?;

    tracing::debug!("{:?}", config);

    let mut device = pos::device::Device { config };
    device.run().await;

    Ok(())
}
