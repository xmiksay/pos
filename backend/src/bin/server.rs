use clap::Parser;
use std::collections::BTreeMap;

use pos::server::message;
use tracing_subscriber::prelude::*;

use tokio::signal::unix::{signal, SignalKind};

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    #[arg(short, long)]
    config: Option<String>,

    #[arg(short, long, action)]
    dummy: bool,
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    // Setup logging
    let fmt_layer = tracing_subscriber::fmt::layer()
        .with_line_number(true)
        .with_writer(std::io::stderr);
    let filter = tracing_subscriber::filter::Targets::new()
        .with_target(env!("CARGO_PKG_NAME"), tracing::Level::TRACE)
        .with_target(env!("CARGO_BIN_NAME"), tracing::Level::TRACE);
    tracing_subscriber::registry()
        .with(fmt_layer)
        .with(filter)
        .init();

    tracing::info!(
        "Commit: {} Version: {}",
        env!("GIT_COMMIT"),
        env!("CARGO_PKG_VERSION")
    );

    // tracing::info!("{:?}", std::env::current_dir());

    let args = Args::parse();

    let config = pos::db::config::Server::create(args.config)?;
    let mut database = pos::db::Database::load(config.clone())?;
    database.repair()?;

    // Creation of context and control structures
    let context = pos::server::Context {
        global_broadcast: tokio::sync::broadcast::Sender::new(config.base.query_size),
        web_broadcast: tokio::sync::broadcast::Sender::new(config.base.query_size),
        device_broadcast: tokio::sync::broadcast::Sender::new(config.base.query_size),
        database,
        devices: BTreeMap::new(),
        clients: BTreeMap::new(),
    };

    let global_sender = context.global_broadcast.clone();
    let _web_sender = context.web_broadcast.clone();

    let context = std::sync::Arc::new(tokio::sync::RwLock::new(context));
    let mut server = pos::server::Server::new(context);

    let server_future = tokio::task::spawn(async move { server.run().await });

    // Create signals
    let mut sig_int = signal(SignalKind::interrupt())?;
    let mut sig_hub = signal(SignalKind::hangup())?;
    let mut sig_quit = signal(SignalKind::quit())?;
    let mut sig_term = signal(SignalKind::terminate())?;

    loop {
        // Reactions to signals
        tokio::select! {
            _ = sig_hub.recv() => {
                global_sender.send(message::Global::Reload)?;
            }
            _ = sig_int.recv() => {
                global_sender.send(message::Global::Stop)?;
                break;
            }
            _ = sig_quit.recv() => {
                global_sender.send(message::Global::Stop)?;
                break;
            }
            _ = sig_term.recv() => {
                global_sender.send(message::Global::Stop)?;
                break;
            }
            /*
            // Example tick message to broadcast
            _ = tokio::time::sleep(std::time::Duration::from_secs(1)) => {
                if web_sender.receiver_count() > 0 {
                    web_sender.send(pos::server::message::Web::UserInfo(None))?;
                } else {
                    tracing::debug!("No connected client");
                }
            }
            */
        }
    }
    server_future.await?
}
