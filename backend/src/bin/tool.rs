use clap::{Parser, Subcommand};
use futures::io::repeat;
use pos::{
    db::{print, Database, LoadSave, PrintTrait},
    device::Connection,
};
use std::io::Write;
use tracing_subscriber::prelude::*;

#[derive(Parser, Debug, Clone)]
#[command(version, about, long_about = None)]
struct Backup {
    #[arg(short, long)]
    file: String,
}

#[derive(Parser, Debug, Clone)]
#[command(version, about, long_about = None)]
struct Auth {
    #[arg(short, long)]
    username: String,
    #[arg(short, long)]
    password: String,
    #[arg(short, long, action)]
    set: bool,
}

#[derive(Parser, Debug, Clone)]
#[command(version, about, long_about = None)]
struct Print {
    #[arg(short, long)]
    pub config: Option<String>,
    #[arg(short, long)]
    pub device: String,
}

#[derive(Subcommand, Clone, Debug)]
pub enum Command {
    GenerateServerConfig,
    GenerateServerDatabase,
    GenerateServerInvoice,
    Report,
    Backup(Backup),
    Restore(Backup),
    Password(Auth),
    Repair,
    Print(Print),
    Test,
}

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
pub struct Cli {
    #[command(subcommand)]
    command: Option<Command>,
}

fn main() -> anyhow::Result<()> {
    let fmt_layer = tracing_subscriber::fmt::layer()
        .with_line_number(true)
        .with_writer(std::io::stderr);
    let filter = tracing_subscriber::filter::Targets::new()
        .with_target(env!("CARGO_PKG_NAME"), tracing::Level::TRACE)
        .with_target(env!("CARGO_BIN_NAME"), tracing::Level::TRACE);
    tracing_subscriber::registry()
        .with(fmt_layer)
        .with(filter)
        .init();

    tracing::info!(
        "Commit: {} Version: {}",
        env!("GIT_COMMIT"),
        env!("CARGO_PKG_VERSION")
    );

    let cli = Cli::parse();
    match &cli.command.unwrap_or(Command::GenerateServerConfig) {
        Command::GenerateServerConfig => {
            let config_path =
                std::env::var("POS_SERVER_CONFIG").unwrap_or(String::from("config.json"));
            let config = if let Ok(config) = pos::db::config::Server::load(&config_path) {
                config
            } else {
                pos::db::config::Server::default()
            };

            config.save()?;
        }
        Command::GenerateServerDatabase => {
            let config_path =
                std::env::var("POS_SERVER_CONFIG").unwrap_or(String::from("config.json"));
            let config = pos::db::config::Server::load(&config_path)?;
            let db = pos::db::Database::dummy(config);
            db.save();
        }
        Command::GenerateServerInvoice => {
            println!(
                "{}",
                serde_json::to_string_pretty(&pos::db::entities::Invoice::default())?
            );
        }
        Command::Report => {
            let date_from = chrono::DateTime::<chrono::Utc>::MIN_UTC;
            let date_to = chrono::DateTime::<chrono::Utc>::MAX_UTC;

            let config = pos::db::config::Server::create(None).unwrap();
            let database = pos::db::Database::load(config)?;
            let report =
                pos::db::report::ReportFull::calculate(&database, &None, date_from, date_to);
            tracing::debug!("{:?}", report);
        }
        Command::Backup(args) => {
            tracing::debug!("Backup");
            let config = pos::db::config::Server::create(None).unwrap();
            let db = pos::db::Database::load(config).unwrap();
            let backup = serde_json::to_string(&db).unwrap();
            std::fs::File::create(&args.file)
                .unwrap()
                .write_all(backup.as_bytes())
                .unwrap();
        }
        Command::Restore(args) => {
            let file = std::fs::File::open(&args.file)?;
            let db: pos::db::Database = serde_json::from_reader(file)?;
            db.save()?;
        }
        Command::Password(args) => {
            let config = pos::db::config::Server::create(None).unwrap();
            let salt = pos::db::entities::User::password_get(&args.password, &config.base.salt);
            tracing::debug!(
                "User: {} Password: {} salt: {}",
                args.username,
                args.password,
                salt
            );

            if args.set {
                // Todo: Store to database
            }
        }
        Command::Repair => {
            let config = pos::db::config::Server::create(None).unwrap();
            let mut db = pos::db::Database::load(config).unwrap();
            db.repair()?;
            db.save()?;
        }

        Command::Print(print) => {
            let config = pos::db::config::Server::create(print.config.clone()).unwrap();
            let device = pos::db::config::Device::load(&print.device)?;
            let mut printer = pos::device::get_connection(&device).unwrap();
            let database = Database::load(config).unwrap();

            if let Some(invoice) = database
                .invoices
                .map
                .get(&uuid::uuid!("b3f9c0fb-6db0-482f-80bd-451b71998c26"))
            {
                let mut data = invoice.print(&database.config, 48);

                tokio::runtime::Builder::new_multi_thread()
                    .enable_all()
                    .build()
                    .unwrap()
                    .block_on(async move {
                        printer
                            .write(pos::server::message::Device::Pos(data.clone()))
                            .await
                            .unwrap()
                    });
            }
        }

        Command::Test => {
            let width = 50;
            let numbers = 15;
            tracing::debug!(
                "{1:0$}x{2:<3}{3:>6} CZK",
                width - numbers,
                "Borovicka",
                2,
                10
            );
        }
    };

    Ok(())
}
