use futures::SinkExt;
use futures_util::StreamExt;
use tokio_serde::formats::*;
use tokio_util::codec::{Framed, LengthDelimitedCodec};

pub mod printer;
pub mod weight;

#[async_trait::async_trait]
pub trait Connection {
    async fn read(&self) -> anyhow::Result<crate::server::message::Device>;
    async fn write(&mut self, message: crate::server::message::Device) -> anyhow::Result<()>;
}

pub struct UsbInteruptDevice {
    pub device: rusb::Device<rusb::Context>,
    pub address: u8,
    pub interface: u8,
    pub handle: rusb::DeviceHandle<rusb::Context>,
    pub delay: u16,
}
impl UsbInteruptDevice {
    pub fn new(vid: u16, pid: u16, delay: u16) -> anyhow::Result<UsbInteruptDevice> {
        let mut context = rusb::Context::new()?;
        let (device, device_desc, handle) = open_device(&mut context, vid, pid)?;

        for n in 0..device_desc.num_configurations() {
            let config_desc = match device.config_descriptor(n) {
                Ok(c) => c,
                Err(_) => continue,
            };

            for interface in config_desc.interfaces() {
                for interface_desc in interface.descriptors() {
                    for endpoint_desc in interface_desc.endpoint_descriptors() {
                        if endpoint_desc.direction() == rusb::Direction::In
                            && endpoint_desc.transfer_type() == rusb::TransferType::Interrupt
                        {
                            return Ok(UsbInteruptDevice {
                                device,
                                address: endpoint_desc.address(),
                                interface: interface_desc.interface_number(),
                                handle,
                                delay,
                            });
                        }
                    }
                }
            }
        }

        Err(anyhow::anyhow!("Address does not found"))
    }
}

#[async_trait::async_trait]
impl Connection for UsbInteruptDevice {
    async fn read(&self) -> anyhow::Result<crate::server::message::Device> {
        tokio::time::sleep(std::time::Duration::from_secs(1)).await;

        let mut buf = [0; 256];
        let timeout = std::time::Duration::from_secs(2);
        let has_kernel_driver = match self.handle.kernel_driver_active(self.interface) {
            Ok(true) => {
                self.handle.detach_kernel_driver(self.interface).ok();
                true
            }
            _ => false,
        };

        self.handle
            .read_interrupt(self.address, &mut buf, timeout)?;

        if has_kernel_driver {
            self.handle.attach_kernel_driver(self.interface).ok();
        }

        Ok(crate::server::message::Device::Weight(
            ((buf[4] as u16) + ((buf[5] as u16) * 256)) as i64,
        ))
    }
    async fn write(&mut self, _message: crate::server::message::Device) -> anyhow::Result<()> {
        Ok(())
    }
}

pub struct UsbBulkDevice {
    pub device: rusb::Device<rusb::Context>,
    pub address: u8,
    pub interface: u8,
    pub handle: rusb::DeviceHandle<rusb::Context>,
    pub delay: u16,
}
impl UsbBulkDevice {
    pub fn new(vid: u16, pid: u16, delay: u16) -> anyhow::Result<UsbBulkDevice> {
        let mut context = rusb::Context::new()?;
        let (device, device_desc, handle) = open_device(&mut context, vid, pid)?;

        for n in 0..device_desc.num_configurations() {
            let config_desc = match device.config_descriptor(n) {
                Ok(c) => c,
                Err(_) => continue,
            };

            for interface in config_desc.interfaces() {
                for interface_desc in interface.descriptors() {
                    for endpoint_desc in interface_desc.endpoint_descriptors() {
                        if endpoint_desc.direction() == rusb::Direction::Out
                            && endpoint_desc.transfer_type() == rusb::TransferType::Bulk
                        {
                            return Ok(UsbBulkDevice {
                                device,
                                address: endpoint_desc.address(),
                                interface: interface_desc.interface_number(),
                                handle,
                                delay,
                            });
                        }
                    }
                }
            }
        }

        Err(anyhow::anyhow!("Address does not found"))
    }
}
#[async_trait::async_trait]
impl Connection for UsbBulkDevice {
    async fn read(&self) -> anyhow::Result<crate::server::message::Device> {
        tokio::time::sleep(std::time::Duration::MAX).await;
        Ok(crate::server::message::Device::Nope)
    }

    async fn write(&mut self, message: crate::server::message::Device) -> anyhow::Result<()> {
        let has_kernel_driver = match self.handle.kernel_driver_active(self.interface) {
            Ok(true) => {
                self.handle.detach_kernel_driver(self.interface).ok();
                true
            }
            _ => false,
        };

        let result = match message {
            crate::server::message::Device::Pos(lines) => {
                for line in lines {
                    self.handle.write_bulk(
                        self.address,
                        &line,
                        std::time::Duration::from_secs(self.delay as u64),
                    )?;

                    tokio::time::sleep(std::time::Duration::from_millis(self.delay as u64)).await;
                }

                Ok(())
            }
            _ => Ok(()),
        };

        if has_kernel_driver {
            self.handle.attach_kernel_driver(self.interface).ok();
        }
        result
    }
}

pub struct NopeDevice {}
#[async_trait::async_trait]
impl Connection for NopeDevice {
    async fn read(&self) -> anyhow::Result<crate::server::message::Device> {
        Ok(crate::server::message::Device::Nope)
    }
    async fn write(&mut self, message: crate::server::message::Device) -> anyhow::Result<()> {
        Ok(())
    }
}

pub struct FileDevice {}
#[async_trait::async_trait]
impl Connection for FileDevice {
    async fn read(&self) -> anyhow::Result<crate::server::message::Device> {
        Ok(crate::server::message::Device::Weight(15))
    }
    async fn write(&mut self, message: crate::server::message::Device) -> anyhow::Result<()> {
        Ok(())
    }
}

fn open_device<T: rusb::UsbContext>(
    context: &mut T,
    vid: u16,
    pid: u16,
) -> anyhow::Result<(
    rusb::Device<T>,
    rusb::DeviceDescriptor,
    rusb::DeviceHandle<T>,
)> {
    let devices = match context.devices() {
        Ok(d) => d,
        Err(err) => return Err(anyhow::anyhow!(err)),
    };

    for device in devices.iter() {
        let device_desc = match device.device_descriptor() {
            Ok(d) => d,
            Err(_) => continue,
        };

        if device_desc.vendor_id() == vid && device_desc.product_id() == pid {
            match device.open() {
                Ok(handle) => return Ok((device, device_desc, handle)),
                Err(e) => panic!("Device found but failed to open: {}", e),
            }
        }
    }

    Err(anyhow::anyhow!("Device does not exist"))
}

pub fn get_connection(config: &crate::db::config::Device) -> anyhow::Result<Box<dyn Connection>> {
    //Err(anyhow::anyhow!("Cannot open device"))
    match &config.connection {
        crate::db::config::DeviceConnection::Nope => Ok(Box::new(NopeDevice {})),
        crate::db::config::DeviceConnection::Path { path, delay } => Ok(Box::new(FileDevice {})),
        crate::db::config::DeviceConnection::UsbInterupt { vid, pid, delay } => Ok(Box::new(
            UsbInteruptDevice::new(vid.clone(), pid.clone(), delay.clone())?,
        )),
        crate::db::config::DeviceConnection::UsbBulk { vid, pid, delay } => Ok(Box::new(
            UsbBulkDevice::new(vid.clone(), pid.clone(), delay.clone())?,
        )),
    }
}

pub struct Device {
    pub config: crate::db::config::Device,
}

impl Device {
    pub async fn run(&mut self) {
        'server: loop {
            tracing::debug!("Connection loop");
            tokio::time::sleep(std::time::Duration::from_secs(1)).await;

            if let Ok(socket) = tokio::net::TcpStream::connect(self.config.server).await {
                tracing::debug!("Connected to server");

                let length_delimited = Framed::new(socket, LengthDelimitedCodec::new());
                let mut stream = tokio_serde::SymmetricallyFramed::new(
                    length_delimited,
                    SymmetricalMessagePack::<crate::server::message::Device>::default(),
                );

                if let Err(err) = stream
                    .send(crate::server::message::Device::Auth(
                        self.config.token.clone(),
                    ))
                    .await
                {
                    tracing::error!("{}", err);
                    continue;
                }

                'device: loop {
                    if let Ok(mut device) = get_connection(&self.config) {
                        device
                            .write(crate::server::message::Device::Pos(vec![
                                "Printer is ready\n\n\n\n\n".as_bytes().to_vec(),
                                vec![27, b'@', 29, b'V', 48, b'\n'],
                            ]))
                            .await
                            .unwrap();

                        loop {
                            tokio::select! {
                                msg = stream.next() => {
                                    if let Err(err) = Device::process_server_msg(&mut stream, &mut device, msg).await {
                                        tracing::error!("Error: {}", err);
                                        continue 'server;
                                    }
                                }
                                msg = device.read() => {
                                    if let Err(err) = Device::process_device_msg(&mut stream, msg).await {
                                        tracing::error!("Error: {}", err);
                                        break 'device;
                                    }
                                }
                            }
                        }
                    } else {
                        tracing::debug!("Device is not present");
                    }
                    tokio::time::sleep(std::time::Duration::from_secs(10)).await;
                }
            }
        }
    }

    async fn process_server_msg(
        socket: &mut tokio_serde::Framed<
            Framed<tokio::net::TcpStream, LengthDelimitedCodec>,
            crate::server::message::Device,
            crate::server::message::Device,
            SymmetricalMessagePack<crate::server::message::Device>,
        >,
        device: &mut Box<dyn Connection>,
        msg: Option<std::io::Result<crate::server::message::Device>>,
    ) -> anyhow::Result<()> {
        tracing::debug!("Message: {:?}", msg);
        match msg {
            Some(Ok(crate::server::message::Device::Ping(value))) => Ok(socket
                .send(crate::server::message::Device::Ping(value))
                .await?),
            Some(Ok(crate::server::message::Device::Pos(lines))) => Ok(device
                .write(crate::server::message::Device::Pos(lines))
                .await?),
            Some(Ok(_)) => Ok(()),
            Some(Err(err)) => Err(err.into()),
            None => Err(anyhow::anyhow!("Empty message")),
        }
    }

    async fn process_device_msg(
        socket: &mut tokio_serde::Framed<
            Framed<tokio::net::TcpStream, LengthDelimitedCodec>,
            crate::server::message::Device,
            crate::server::message::Device,
            SymmetricalMessagePack<crate::server::message::Device>,
        >,
        msg: anyhow::Result<crate::server::message::Device>,
    ) -> anyhow::Result<()> {
        match msg {
            Ok(crate::server::message::Device::Weight(weight)) => Ok(socket
                .send(crate::server::message::Device::Weight(weight))
                .await?),
            Ok(_) => Ok(()),
            Err(err) => Err(err),
        }
    }
}
