use std::ops::Mul;

use chrono::prelude::{DateTime, Utc};
use rust_decimal::Decimal;
use serde::{Deserialize, Deserializer, Serialize, Serializer};

use crate::{db::PrintTrait, server::message::Print};

use super::config::DeviceType;

fn deserialize_null_default<'de, D, T>(deserializer: D) -> Result<T, D::Error>
where
    D: Deserializer<'de>,
    T: Default + Deserialize<'de>,
{
    let opt = Option::<T>::deserialize(deserializer)?;
    Ok(opt.unwrap_or_default())
}

#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct Tag {
    pub uuid: uuid::Uuid,
    pub name: String,
    pub order: u16,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct Room {
    pub uuid: uuid::Uuid,
    pub name: String,
    pub order: u16,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub enum Unit {
    #[default]
    Piece, // (countable)
    Volume, // (ml)
    Weight, // (g)
}

#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct Supplier {
    pub uuid: uuid::Uuid,
    pub name: String,
    pub data: serde_json::Value,
    pub note: String,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct Meta {
    pub uuid: uuid::Uuid,
    pub name: String,
    pub tags: Vec<uuid::Uuid>,
    pub hide: bool,

    pub unit: Unit,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct Product {
    pub uuid: uuid::Uuid,
    pub name: String,
    pub tags: Vec<uuid::Uuid>,
    pub amount: i64, // Number of units from meta
    pub meta_uuid: Option<uuid::Uuid>,
    pub hide: bool,
    // Sel price
    pub price: rust_decimal::Decimal,
    pub print_prepare: bool,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct Thing {
    pub uuid: uuid::Uuid,
    pub name: String,
    pub meta_uuid: uuid::Uuid,
    pub tags: Vec<uuid::Uuid>,
    pub hide: bool,

    pub capacity: i64,
    pub density: f32,
    pub empty: i64,
    pub full: i64,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct Table {
    pub uuid: uuid::Uuid,
    pub room_uuid: uuid::Uuid,
    pub name: String,
    pub order: u16,
    #[serde(default = "crate::util::now")]
    pub last_activity: DateTime<Utc>,
    pub user_uuid: Option<uuid::Uuid>,
    pub invoice_uuid: Option<uuid::Uuid>,
    #[serde(default, deserialize_with = "deserialize_null_default")]
    pub items: Vec<TableItem>,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct TableItem {
    pub product_uuid: uuid::Uuid,
    pub quantity: i64,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize, Eq, PartialEq, Hash)]
#[serde(rename_all = "lowercase")]
pub enum Payment {
    #[default]
    Cash = 1,
    QRCode = 2,
    Card = 3,
    Other = 0,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct Invoice {
    pub uuid: uuid::Uuid,
    // User who serve
    pub user_uuid: uuid::Uuid,
    // Table where guest sit
    pub table_uuid: Option<uuid::Uuid>,
    // Must be usable for variable symbol
    pub sequence: Option<String>,
    // Date of creation of invoice
    pub date: DateTime<Utc>,
    // Payment method
    pub payment: Payment,
    // When is the order paid
    pub paid_date: Option<DateTime<Utc>>,
    // How much is paid (tips)
    pub paid_amount: rust_decimal::Decimal,
    // If the bill is printed
    pub printed: Option<DateTime<Utc>>,
    // If the order is canceled
    pub is_canceled: bool,
    // If the order is locked - unabled to edit by normal user
    pub is_locked: bool,

    #[serde(default)]
    pub items: Vec<InvoiceItem>,
    pub note: String,
}

impl super::DaySplitTrait for Invoice {
    fn get_day(&self) -> chrono::NaiveDate {
        self.date.date_naive()
    }

    fn get_uuid(&self) -> uuid::Uuid {
        self.uuid
    }
}

impl super::PrintTrait for Invoice {
    fn print(&self, config: &crate::db::config::Server, size: usize) -> Vec<Vec<u8>> {
        let mut result = config.info.print(config, size);

        result.push(
            format!(
                "{:<width$} {}\n\n\n",
                "Ciselna rada: ",
                self.sequence.clone().unwrap_or(String::new()),
                width = size - 20,
            )
            .as_bytes()
            .to_vec(),
        );
        result.push(
            format!(
                "{:^width$}  Cena  Pocet Celkem\n",
                "Nazev",
                width = size - 20
            )
            .as_bytes()
            .to_vec(),
        );

        let mut total = Decimal::ZERO;
        for item in self.items.iter() {
            let mut print_item = item.print(config, size);
            total += item.price * Decimal::from(item.quantity);
            result.append(&mut print_item);
        }

        result.push(Invoice::sep("-", size, 1));
        result.push(
            format!(
                "{:<width$}{:>6} CZK\n",
                "Celkem: ",
                total,
                width = size - 10
            )
            .as_bytes()
            .to_vec(),
        );
        result.push(Invoice::sep("-", size, 1));
        result.push(Invoice::sep(" ", size, 4));
        result.push(Invoice::cut());
        result
    }
}

#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct InvoiceItem {
    pub product_uuid: Option<uuid::Uuid>,
    pub name: String, // Derived from product
    pub quantity: i64,
    pub price: rust_decimal::Decimal,
}

impl PrintTrait for InvoiceItem {
    fn print(&self, config: &crate::db::config::Server, size: usize) -> Vec<Vec<u8>> {
        let mut name = self.name.clone();
        name.truncate(size - 21);
        vec![format!(
            "{:width$}{:>6}{:>4}x{:>6} CZK\n",
            unidecode::unidecode(&name),
            self.price,
            self.quantity,
            self.price * Decimal::from(self.quantity),
            width = size - 21
        )
        .as_bytes()
        .to_vec()]
    }
}

#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub enum SourceKind {
    #[default]
    New,
    Prepare,
    Ordered,
    Delivered,
    Paid,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct Source {
    pub uuid: uuid::Uuid,
    pub user_uuid: uuid::Uuid,
    pub kind: SourceKind,
    pub number: String,
    pub supplier_uuid: Option<uuid::Uuid>,
    pub date: DateTime<Utc>,
    pub other: serde_json::Value,
    #[serde(default, deserialize_with = "deserialize_null_default")]
    pub items: Vec<SourceItem>,
    pub note: String,
}

impl super::DaySplitTrait for Source {
    fn get_day(&self) -> chrono::NaiveDate {
        self.date.date_naive()
    }

    fn get_uuid(&self) -> uuid::Uuid {
        self.uuid
    }
}

#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct SourceItem {
    pub thing_uuid: Option<uuid::Uuid>,
    pub name: String,
    pub quantity: i64,
    pub price: rust_decimal::Decimal,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct Inventory {
    pub uuid: uuid::Uuid,
    pub user: uuid::Uuid,
    pub date: DateTime<Utc>,
    pub prev: Option<uuid::Uuid>,
    #[serde(default, deserialize_with = "deserialize_null_default")]
    pub items: Vec<InventoryItem>,
}

impl super::DaySplitTrait for Inventory {
    fn get_day(&self) -> chrono::NaiveDate {
        self.date.date_naive()
    }

    fn get_uuid(&self) -> uuid::Uuid {
        self.uuid
    }
}

#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct InventoryItem {
    pub meta_uuid: uuid::Uuid,
    pub amount: u64,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct User {
    pub uuid: uuid::Uuid,
    pub nick: String,
    pub password: String,
    pub name: String,
    pub pin: String,
    pub is_admin: bool,
    pub is_enabled: bool,
    pub last_activity: Option<DateTime<Utc>>,
}

impl User {
    pub fn password_get(password: &str, salt: &str) -> String {
        use blake2::{Blake2s256, Digest};

        let mut hasher = Blake2s256::new();
        hasher.update(password);
        hasher.update(salt);
        hex::encode(hasher.finalize())
    }

    pub fn password_check(&self, password: &str, salt: &str) -> bool {
        let checked = User::password_get(password, salt);
        tracing::debug!("Login check: {password} - {checked}");
        self.password == checked
    }
}

#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct Token {
    pub value: String,
    pub user_uuid: uuid::Uuid,
    pub locked: bool,
    pub expiration: DateTime<Utc>,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct Device {
    pub uuid: uuid::Uuid,
    pub name: String,
    pub token: String,
    pub kind: DeviceType,
}
