use std::net::{Ipv4Addr, SocketAddrV4};

use super::{LoadSave, PrintTrait};
use rand::Rng;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct Base {
    pub config_path: String,
    pub data_path: String,
    pub frontend_path: String,
    pub salt: String,
    pub listen_address: String,
    pub query_size: usize,
    pub port_http: SocketAddrV4,
    pub port_tcp: SocketAddrV4,
}

impl Default for Base {
    fn default() -> Self {
        let mut rng = rand::thread_rng();

        Self {
            config_path: String::from("./data/server.json"),
            data_path: String::from("./data/"),
            listen_address: String::from("0.0.0.0"),
            frontend_path: String::from("../frontend/dist/"),
            salt: hex::encode((0..32).map(|_| rng.gen::<u8>()).collect::<Vec<u8>>()),
            query_size: 64,
            port_http: SocketAddrV4::new(Ipv4Addr::BROADCAST, 3030),
            port_tcp: SocketAddrV4::new(Ipv4Addr::BROADCAST, 3031),
        }
    }
}

impl Base {
    pub fn get_logo_path(&self) -> String {
        std::path::Path::new(&self.data_path)
            .join("logo.bin")
            .to_str()
            .unwrap()
            .to_string()
    }
    pub fn get_definition_path(&self) -> String {
        std::path::Path::new(&self.data_path)
            .join("definition.json")
            .to_str()
            .unwrap()
            .to_string()
    }
    pub fn get_tables_path(&self) -> String {
        std::path::Path::new(&self.data_path)
            .join("tables.json")
            .to_str()
            .unwrap()
            .to_string()
    }
    pub fn get_auth_path(&self) -> String {
        std::path::Path::new(&self.data_path)
            .join("auth.json")
            .to_str()
            .unwrap()
            .to_string()
    }

    pub fn get_invoices_path(&self) -> String {
        std::path::Path::new(&self.data_path)
            .join("invoices")
            .to_str()
            .unwrap()
            .to_string()
    }

    pub fn get_invoice_path(&self, date: &chrono::NaiveDate) -> String {
        std::path::Path::new(&self.get_invoices_path())
            .join(date.format("%Y-%m-%d.json").to_string())
            .to_str()
            .unwrap()
            .to_string()
    }

    pub fn get_sources_path(&self) -> String {
        std::path::Path::new(&self.data_path)
            .join("sources")
            .to_str()
            .unwrap()
            .to_string()
    }

    pub fn get_source_path(&self, date: &chrono::NaiveDate) -> String {
        std::path::Path::new(&self.get_sources_path())
            .join(date.format("%Y-%m-%d.json").to_string())
            .to_str()
            .unwrap()
            .to_string()
    }

    pub fn get_inventories_path(&self) -> String {
        std::path::Path::new(&self.data_path)
            .join("inventories")
            .to_str()
            .unwrap()
            .to_string()
    }

    pub fn get_inventory_path(&self, date: &chrono::NaiveDate) -> String {
        std::path::Path::new(&self.get_inventories_path())
            .join(date.format("%Y-%m-%d.json").to_string())
            .to_str()
            .unwrap()
            .to_string()
    }
}

#[derive(Clone, Debug, Default, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct Address {
    pub name: String,
    pub address: String,
    pub city: String,
    pub zip: String,
}

impl PrintTrait for Address {
    fn print(&self, config: &crate::db::config::Server, size: usize) -> Vec<Vec<u8>> {
        vec![format!(
            "{}\n{}\n{} {}\n\n",
            self.name, self.address, self.city, self.zip
        )
        .as_bytes()
        .to_vec()]
    }
}

#[derive(Clone, Debug, Default, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct Bank {
    pub prefix: String,
    pub number: String,
    pub code: String,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct Info {
    pub name: String,
    pub operator: Address,
    pub store: Address,
    pub ic: String,
    pub bank: Bank,
}

impl PrintTrait for Info {
    fn print(&self, config: &crate::db::config::Server, size: usize) -> Vec<Vec<u8>> {
        let mut store_address_print = self.store.print(config, size);
        let mut operator_address_print = self.operator.print(config, size);
        let mut result = Vec::new();
        result.push(format!("{}\n", self.name).as_bytes().to_vec());
        result.push("Provozovna\n".as_bytes().to_vec());
        result.append(&mut store_address_print);
        result.push("Provozovatel\n".as_bytes().to_vec());
        result.append(&mut operator_address_print);
        result.push(format!("IC: {}\n\n", self.ic).as_bytes().to_vec());

        result
    }
}

#[derive(Clone, Debug, Default, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct Setting {
    pub printer_bill: Option<uuid::Uuid>,
    pub printer_kitchen: Option<uuid::Uuid>,
    pub auto_lock: i64,
    pub retention_days: i64,
    pub hour_open: chrono::NaiveTime,
    pub hour_close: chrono::NaiveTime,
    pub last_inventory: Option<uuid::Uuid>,
    pub default_print: bool,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct Server {
    pub base: Base,
    pub info: Info,
    pub setting: Setting,
}
impl LoadSave for Server {}

impl Server {
    pub fn save(&self) -> anyhow::Result<()> {
        LoadSave::save(self, &self.base.config_path)
    }

    pub fn create(config_path: Option<String>) -> anyhow::Result<Server> {
        let config_path = config_path
            .unwrap_or(std::env::var("POS_SERVER_CONFIG").unwrap_or(String::from("config.json")));

        tracing::info!("Loading config from: {}", config_path);

        match crate::db::config::Server::load(&config_path) {
            Ok(config) => Ok(config),
            Err(err) => {
                tracing::error!("{}", err);
                Err(anyhow::anyhow!("Cannot load config without force"))
            }
        }
    }
}

#[derive(Clone, Debug, Default, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub enum DeviceType {
    #[default]
    Weight,
    Pos(u16), // printer line width
}

#[derive(Clone, Debug, Default, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub enum DeviceConnection {
    #[default]
    Nope,
    Path {
        path: String,
        delay: u16,
    },
    UsbInterupt {
        vid: u16,
        pid: u16,
        delay: u16,
    },
    UsbBulk {
        vid: u16,
        pid: u16,
        delay: u16,
    },
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct Device {
    pub server: SocketAddrV4,
    pub token: String,
    pub device: DeviceType,
    pub connection: DeviceConnection,
}

impl LoadSave for Device {}

impl Default for Device {
    fn default() -> Self {
        Self {
            server: SocketAddrV4::new(Ipv4Addr::LOCALHOST, 3030),
            token: String::from("token"),
            device: DeviceType::Weight,
            connection: DeviceConnection::UsbInterupt {
                vid: 0x0922,
                pid: 0x8003,
                delay: 0,
            },
        }
    }
}
