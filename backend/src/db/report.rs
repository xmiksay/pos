use std::collections::{BTreeMap, HashMap};

use rust_decimal::Decimal;
use serde::Serialize;

use super::entities::Payment;

#[derive(Debug, Default, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ReportItem {
    pub amount: Decimal,
    pub real: Decimal,
    pub paid: Decimal,
}

#[derive(Debug, Default, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ReportFull {
    pub date_from: chrono::DateTime<chrono::Utc>,
    pub date_to: chrono::DateTime<chrono::Utc>,
    pub invoice_user: std::collections::HashMap<uuid::Uuid, ReportItem>,
    pub invoice_payment: std::collections::HashMap<super::entities::Payment, ReportItem>,
    pub invoice_meta: std::collections::HashMap<uuid::Uuid, ReportItem>,
    pub invoice_product: std::collections::HashMap<uuid::Uuid, ReportItem>,
    pub invoice_total: ReportItem,

    pub source_user: std::collections::HashMap<uuid::Uuid, ReportItem>,
    pub source_meta: std::collections::HashMap<uuid::Uuid, ReportItem>,
    pub source_supplier: std::collections::HashMap<uuid::Uuid, ReportItem>,
    pub source_thing: std::collections::HashMap<uuid::Uuid, ReportItem>,
    pub source_total: ReportItem,

    pub total: ReportItem,
}

impl ReportFull {
    pub fn calculate(
        db: &super::Database,
        user: &Option<super::entities::User>,
        date_from: chrono::DateTime<chrono::Utc>,
        date_to: chrono::DateTime<chrono::Utc>,
    ) -> ReportFull {
        let mut report = ReportFull {
            date_from: date_from.clone(),
            date_to: date_to.clone(),
            ..ReportFull::default()
        };

        for invoice in db.invoices.map.values().filter(|i| {
            if let Some(paid_date) = &i.paid_date {
                paid_date.ge(&date_from)
                    && paid_date.le(&date_to)
                    && user.as_ref().map(|u| u.uuid == i.user_uuid).unwrap_or(true)
            } else {
                false
            }
        }) {
            report.process_invoice(db, invoice);
        }

        for source in db.sources.map.values().filter(|i| {
            i.date.ge(&date_from)
                && i.date.le(&date_to)
                && user.as_ref().map(|u| u.uuid == i.user_uuid).unwrap_or(true)
                && i.kind != super::entities::SourceKind::New
        }) {
            report.process_source(db, source);
        }

        // Output - Input => Profit
        report.total.real = report.invoice_total.real - report.source_total.real;
        // Paid - Real => Extra profit
        report.total.paid = report.invoice_total.paid - report.invoice_total.real;

        report
    }

    fn invoice_sum(acc: Decimal, item: &super::entities::InvoiceItem) -> Decimal {
        acc + item.price * Decimal::new(item.quantity, 0)
    }

    fn invoice_add_all(item: &mut ReportItem, invoice: &super::entities::Invoice) {
        item.amount += Decimal::new(1, 0);
        item.paid += invoice.paid_amount;
        item.real += invoice
            .items
            .iter()
            .fold(rust_decimal::Decimal::default(), ReportFull::invoice_sum);
    }

    fn process_invoice(&mut self, db: &super::Database, invoice: &super::entities::Invoice) {
        tracing::debug!("Procesing invoice: {:?}", invoice);

        self.process_invoice_user(invoice);
        self.process_invoice_payment(invoice);
        self.process_invoice_product(db, invoice);
        self.process_invoice_total(invoice);
    }

    fn process_invoice_user(&mut self, invoice: &super::entities::Invoice) {
        self.invoice_user.entry(invoice.user_uuid).or_default();

        if let Some(item) = self.invoice_user.get_mut(&invoice.user_uuid) {
            ReportFull::invoice_add_all(item, invoice);
        }
    }

    fn process_invoice_payment(&mut self, invoice: &super::entities::Invoice) {
        if !self.invoice_payment.contains_key(&invoice.payment) {
            self.invoice_payment
                .insert(invoice.payment.clone(), ReportItem::default());
        }

        if let Some(item) = self.invoice_payment.get_mut(&invoice.payment) {
            ReportFull::invoice_add_all(item, invoice);
        }
    }

    /**
     * Proces all invoice item and separe them to product/metas calculator
     */
    fn process_invoice_product(
        &mut self,
        db: &super::Database,
        invoice: &super::entities::Invoice,
    ) {
        for item in invoice.items.iter() {
            let quantity = Decimal::new(item.quantity, 0);

            // Only items with produc
            if let Some(product_uuid) = item.product_uuid {
                if let Ok(product) = db.product_get(&product_uuid) {
                    // Create product report
                    self.invoice_product.entry(product_uuid).or_default();

                    if let Some(report_item) = self.invoice_product.get_mut(&product_uuid) {
                        report_item.amount += quantity;
                        report_item.real += item.price * quantity;
                    }

                    // Create meta report
                    if let Some(meta_uuid) = product.meta_uuid {
                        self.invoice_meta.entry(meta_uuid).or_default();

                        if let Some(report_item) = self.invoice_meta.get_mut(&meta_uuid) {
                            report_item.amount += quantity * Decimal::new(product.amount, 0);
                            report_item.real += item.price * quantity;
                        }
                    }
                }
            }
        }
    }

    fn process_invoice_total(&mut self, invoice: &super::entities::Invoice) {
        ReportFull::invoice_add_all(&mut self.invoice_total, invoice)
    }

    fn source_sum(acc: Decimal, item: &super::entities::SourceItem) -> Decimal {
        acc + item.price * Decimal::new(item.quantity, 0)
    }

    fn source_add_all(item: &mut ReportItem, source: &super::entities::Source) {
        item.amount += Decimal::new(1, 0);
        item.real += source
            .items
            .iter()
            .fold(rust_decimal::Decimal::default(), ReportFull::source_sum);
    }

    fn process_source(&mut self, db: &super::Database, source: &super::entities::Source) {
        tracing::debug!("Procesing source: {:?}", source);

        self.process_source_user(source);
        self.process_source_supplier(source);
        self.process_source_thing(db, source);
        self.process_source_total(source);
    }

    fn process_source_user(&mut self, source: &super::entities::Source) {
        self.source_user.entry(source.user_uuid).or_default();

        if let Some(item) = self.source_user.get_mut(&source.user_uuid) {
            ReportFull::source_add_all(item, source);
        }
    }

    fn process_source_supplier(&mut self, source: &super::entities::Source) {
        if let Some(supplier_uuid) = source.supplier_uuid {
            self.source_supplier.entry(supplier_uuid).or_default();

            if let Some(item) = self.source_supplier.get_mut(&supplier_uuid) {
                ReportFull::source_add_all(item, source);
            }
        }
    }

    fn process_source_thing(&mut self, db: &super::Database, source: &super::entities::Source) {
        for item in source.items.iter() {
            let quantity = Decimal::new(item.quantity, 0);

            // Only items with produc
            if let Some(thing_uuid) = item.thing_uuid {
                if let Ok(thing) = db.thing_get(&thing_uuid) {
                    // Create product report
                    self.source_thing.entry(thing_uuid).or_default();

                    if let Some(report_item) = self.source_thing.get_mut(&thing_uuid) {
                        report_item.amount += quantity;
                        report_item.real += item.price * quantity;
                    }

                    // Create meta report
                    self.source_meta.entry(thing.meta_uuid).or_default();

                    if let Some(report_item) = self.source_meta.get_mut(&thing.meta_uuid) {
                        report_item.amount += quantity * Decimal::new(thing.capacity, 0);
                        report_item.real += item.price * quantity;
                    }
                }
            }
        }
    }
    fn process_source_total(&mut self, source: &super::entities::Source) {
        ReportFull::source_add_all(&mut self.source_total, source)
    }
}

pub type ReportDailyItem = HashMap<Payment, rust_decimal::Decimal>;

#[derive(Debug, Default, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ReportDaily {
    pub date_from: chrono::DateTime<chrono::Utc>,
    pub date_to: chrono::DateTime<chrono::Utc>,
    pub days: BTreeMap<chrono::NaiveDate, ReportDailyItem>,
}
impl ReportDaily {
    pub fn calculate(
        db: &super::Database,
        user: &Option<super::entities::User>,
        date_from: chrono::DateTime<chrono::Utc>,
        date_to: chrono::DateTime<chrono::Utc>,
    ) -> ReportDaily {
        let mut report = ReportDaily {
            date_from: date_from.clone(),
            date_to: date_to.clone(),
            ..ReportDaily::default()
        };

        for invoice in db.invoices.map.values() {
            let sum = invoice
                .items
                .iter()
                .fold(rust_decimal::Decimal::default(), ReportFull::invoice_sum);
            let date = invoice.date.date_naive();
            if let Some(day) = report.days.get_mut(&date) {
                if let Some(prev) = day.get(&invoice.payment) {
                    day.insert(invoice.payment.clone(), sum + prev);
                } else {
                    day.insert(invoice.payment.clone(), sum);
                }
            } else {
                report.days.insert(
                    date.clone(),
                    HashMap::from([(invoice.payment.clone(), sum)]),
                );
            }
            let prev = report
                .days
                .get(&date)
                .map(|d| d.get(&invoice.payment))
                .unwrap_or_default();
        }

        report
    }
}
