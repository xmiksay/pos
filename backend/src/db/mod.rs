use anyhow::Context;
use chrono::{DateTime, Utc};

use entities::Product;
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use std::collections::BTreeMap;

pub mod config;
pub mod default;
pub mod entities;
pub mod print;
pub mod report;

pub use print::PrintTrait;

pub trait LoadSave {
    fn load(path: &str) -> anyhow::Result<Self>
    where
        Self: Sized + serde::de::DeserializeOwned,
    {
        crate::util::json_load::<Self>(path)
    }

    fn save(&self, path: &str) -> anyhow::Result<()>
    where
        Self: Sized + serde::Serialize,
    {
        crate::util::json_save(path, self)
    }
}

#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct Definition {
    pub tags: BTreeMap<uuid::Uuid, entities::Tag>,
    pub suppliers: BTreeMap<uuid::Uuid, entities::Supplier>,
    pub rooms: BTreeMap<uuid::Uuid, entities::Room>,
    pub metas: BTreeMap<uuid::Uuid, entities::Meta>,
    pub products: BTreeMap<uuid::Uuid, entities::Product>,
    pub things: BTreeMap<uuid::Uuid, entities::Thing>,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct Auth {
    pub users: BTreeMap<uuid::Uuid, entities::User>,
    pub tokens: BTreeMap<String, entities::Token>,
    pub devices: BTreeMap<uuid::Uuid, entities::Device>,
}

pub type Tables = BTreeMap<uuid::Uuid, entities::Table>;

pub trait DaySplitTrait {
    fn get_uuid(&self) -> uuid::Uuid;
    fn get_day(&self) -> chrono::NaiveDate;
}

#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
pub struct DaySplit<T>
where
    T: PartialEq + Clone,
{
    // All invoices sorted
    pub map: BTreeMap<uuid::Uuid, T>,
    pub day: BTreeMap<chrono::NaiveDate, Vec<uuid::Uuid>>,
}

impl<T> DaySplit<T>
where
    T: std::fmt::Debug + Clone + PartialEq + Serialize + DaySplitTrait,
{
    pub fn save(&self, date: &chrono::NaiveDate, path: &str) -> anyhow::Result<()> {
        if let Some(day) = self.day.get(date) {
            tracing::debug!("Saving for {}", date);

            let items = BTreeMap::<uuid::Uuid, T>::from_iter(
                day.iter()
                    .filter_map(|d| self.map.get(d))
                    .map(|d| (d.get_uuid(), d.clone())),
            );
            return crate::util::json_save(path, &items);
        }
        Err(anyhow::anyhow!("err"))
    }

    pub fn save_all<F>(&self, get_path: F) -> anyhow::Result<()>
    where
        F: Fn(&chrono::NaiveDate) -> String,
    {
        for day in self.day.keys() {
            self.save(day, &get_path(day))?;
        }
        Ok(())
    }

    pub fn reindex(&mut self) {
        // Create fast index by day table
        for item in self.map.values_mut() {
            self.day.entry(item.get_day()).or_default();

            if let Some(day) = self.day.get_mut(&item.get_day()) {
                day.push(item.get_uuid())
            }
        }
    }
}

pub type Invoices = DaySplit<entities::Invoice>;
pub type Sources = DaySplit<entities::Source>;
pub type Inventories = DaySplit<entities::Inventory>;

#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct Database {
    pub config: config::Server,
    pub definition: Definition,
    pub auth: Auth,
    pub tables: Tables,
    pub invoices: Invoices,
    pub sources: Sources,
    pub inventories: Inventories,
    pub version: String,
}

impl Database {
    pub fn load(config: crate::db::config::Server) -> anyhow::Result<Self> {
        let definition = Database::definition_load(&config.base.get_definition_path())?;
        let auth = Database::auth_load(&config.base.get_auth_path())?;
        let tables = Database::tables_load(&config.base.get_tables_path())?;
        let invoices = Database::invoices_load(&config)?;
        let sources = Database::sources_load(&config)?;
        let inventories = Database::inventories_load(&config)?;

        Ok(Database {
            config,
            definition,
            auth,
            tables,
            invoices,
            sources,
            inventories,
            version: env!("CARGO_PKG_VERSION").into(),
        })
    }

    pub fn save(&self) -> anyhow::Result<()> {
        std::fs::create_dir_all(self.config.base.get_invoices_path())?;
        std::fs::create_dir_all(self.config.base.get_sources_path())?;

        self.invoices
            .save_all(|day: &chrono::NaiveDate| -> String {
                self.config.base.get_invoice_path(day)
            })?;
        self.sources.save_all(|day: &chrono::NaiveDate| -> String {
            self.config.base.get_source_path(day)
        })?;

        self.definition_save()?;
        self.auth_save()?;
        self.tables_save()?;

        self.config.save()?;

        Ok(())
    }

    pub fn definition_load(path: &str) -> anyhow::Result<Definition> {
        crate::util::json_load(path)
    }
    pub fn definition_save(&self) -> anyhow::Result<()> {
        tracing::debug!("Saving definitions");
        crate::util::json_save(&self.config.base.get_definition_path(), &self.definition)
    }

    pub fn auth_load(path: &str) -> anyhow::Result<Auth> {
        crate::util::json_load(path)
    }
    pub fn auth_save(&self) -> anyhow::Result<()> {
        tracing::debug!("Saving auth");
        let mut auth = self.auth.clone();
        auth.tokens = self
            .auth
            .tokens
            .iter()
            .filter_map(|token| {
                if token.1.locked {
                    Some((token.0.clone(), token.1.clone()))
                } else {
                    None
                }
            })
            .collect();
        crate::util::json_save(&self.config.base.get_auth_path(), &auth)
    }

    pub fn tables_load(path: &str) -> anyhow::Result<Tables> {
        crate::util::json_load(path)
    }

    pub fn tables_save(&self) -> anyhow::Result<()> {
        tracing::debug!("Saving tables");
        crate::util::json_save(&self.config.base.get_tables_path(), &self.tables)
    }

    pub fn load_by_day<T>(path: String) -> anyhow::Result<DaySplit<T>>
    where
        T: PartialEq
            + Clone
            + Default
            + std::fmt::Debug
            + Serialize
            + DeserializeOwned
            + DaySplitTrait,
    {
        let mut items: DaySplit<T> = DaySplit::default();
        let day_path = std::path::Path::new(&path);
        if day_path.exists() {
            for item in day_path.read_dir()? {
                if let Ok(entry) = item {
                    if entry.path().is_file() {
                        let mut entries = crate::util::json_load(entry.path().to_str().unwrap())?;
                        items.map.append(&mut entries);
                    }
                }
            }
        }

        items.reindex();

        Ok(items)
    }

    pub fn invoices_load(server: &config::Server) -> anyhow::Result<Invoices> {
        Self::load_by_day(server.base.get_invoices_path())
    }

    pub fn sources_load(server: &config::Server) -> anyhow::Result<Sources> {
        Self::load_by_day(server.base.get_sources_path())
    }

    pub fn inventories_load(server: &config::Server) -> anyhow::Result<Inventories> {
        Self::load_by_day(server.base.get_inventories_path())
    }

    /**
     * Assign definition and store it to disk
     */
    pub fn definition_set(&mut self, definition: Definition) -> anyhow::Result<()> {
        self.definition = definition;
        crate::util::json_save(&self.config.base.get_definition_path(), &self.definition)
    }

    pub fn product_get<'a>(&'a self, uuid: &uuid::Uuid) -> anyhow::Result<&'a entities::Product> {
        self.definition
            .products
            .get(uuid)
            .context("Product not found")
    }

    pub fn thing_get<'a>(&'a self, uuid: &uuid::Uuid) -> anyhow::Result<&'a entities::Thing> {
        self.definition
            .things
            .get(uuid)
            .context("Product not found")
    }

    pub fn table_get<'a>(&'a self, uuid: &uuid::Uuid) -> anyhow::Result<&'a entities::Table> {
        self.tables.get(uuid).context("Table not found")
    }

    pub fn table_save(&self) -> anyhow::Result<()> {
        crate::util::json_save(&self.config.base.get_tables_path(), &self.tables)
    }

    /**
     * Assign definition and store it to disk
     */
    pub fn tables_set(
        &mut self,
        tables: BTreeMap<uuid::Uuid, entities::Table>,
    ) -> anyhow::Result<()> {
        self.tables = tables;
        self.table_save();
        Ok(())
    }

    // Return diff
    pub fn table_sync(
        &mut self,
        value: entities::Table,
    ) -> anyhow::Result<Vec<entities::TableItem>> {
        let table = if let Some(_table) = self.tables.get(&value.uuid) {
            entities::Table {
                last_activity: chrono::offset::Utc::now(),
                ..value
            }
        } else {
            entities::Table {
                last_activity: chrono::offset::Utc::now(),
                ..value
            }
        };
        self.tables.insert(table.uuid, table);
        crate::util::json_save(&self.config.base.get_tables_path(), &self.tables).unwrap();
        Ok(Vec::new())
    }

    pub fn table_remove(&mut self, uuid: uuid::Uuid) -> anyhow::Result<()> {
        self.tables.remove(&uuid);
        self.table_save()
    }

    pub fn invoice_get<'a>(&'a self, uuid: &uuid::Uuid) -> anyhow::Result<&'a entities::Invoice> {
        self.invoices.map.get(uuid).context("Invoice not found")
    }

    // Return diff
    pub fn invoice_sync(
        &mut self,
        value: entities::Invoice,
    ) -> anyhow::Result<Vec<entities::InvoiceItem>> {
        let date = value.date.date_naive();
        self.invoices.map.insert(value.uuid, value);
        self.invoices.reindex();
        self.invoices
            .save(&date, &self.config.base.get_invoice_path(&date))?;

        Ok(Vec::new())
    }

    pub fn invoice_filter<'a>(&self, user: &Option<entities::User>) -> Vec<&entities::Invoice> {
        let now = chrono::Utc::now().date_naive();
        self.invoices
            .map
            .values()
            .filter(|invoice| {
                // Not older then
                ((now - invoice.date.date_naive()).num_days()
                        < self.config.setting.retention_days)
                        // Owned by user
                        ||
                            user.as_ref()
                            .is_some_and(|u| u.uuid == invoice.user_uuid)
                    // unpaid and not canceled
                    ||  !invoice.is_canceled || invoice.paid_date.is_none()
            })
            .collect()
    }

    pub fn source_get<'a>(&'a self, uuid: &uuid::Uuid) -> anyhow::Result<&'a entities::Source> {
        self.sources.map.get(uuid).context("Source not found")
    }

    pub fn source_remove(&mut self, source: &entities::Source) -> anyhow::Result<()> {
        let day = &source.date.date_naive();
        self.sources.map.remove(&source.uuid);
        self.sources
            .save(day, &self.config.base.get_source_path(day))
    }

    pub fn source_sync(&mut self, source: entities::Source) -> anyhow::Result<()> {
        let date = source.date.date_naive();
        self.sources.map.insert(source.uuid, source);
        self.sources.reindex();
        self.sources
            .save(&date, &self.config.base.get_source_path(&date))
    }

    pub fn source_filter(&self) -> BTreeMap<uuid::Uuid, entities::Source> {
        let now = chrono::Utc::now().date_naive();
        self.sources
            .map
            .iter()
            .filter(|(_uuid, source)|
                // Not older then
                (now - source.date.date_naive()).num_days() < self.config.setting.retention_days)
            .map(|(uuid, item)| (*uuid, item.clone()))
            .collect()
    }

    pub fn repair(&mut self) -> anyhow::Result<()> {
        // Repair products
        for invoice_item in self
            .invoices
            .map
            .values()
            .map(|ii| ii.items.iter())
            .flatten()
        {
            if let Some(product_uuid) = &invoice_item.product_uuid {
                if !self.definition.products.contains_key(product_uuid) {
                    self.definition.products.insert(
                        product_uuid.clone(),
                        Product {
                            uuid: product_uuid.clone(),
                            name: invoice_item.name.clone(),
                            price: invoice_item.price.clone(),
                            ..Default::default()
                        },
                    );
                }
            }
        }

        Ok(())
    }
}
