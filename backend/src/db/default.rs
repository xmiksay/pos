impl super::Database {
    pub fn dummy(config: crate::db::config::Server) -> super::Database {
        let mut db = super::Database {
            config,
            ..super::Database::default()
        };
        let user_uuid = uuid::Uuid::new_v4();
        db.auth.users.insert(
            user_uuid,
            super::entities::User {
                uuid: user_uuid,
                is_admin: true,
                is_enabled: true,
                nick: String::from("admin"),
                password: String::from("admin"),
                pin: String::from("1234"),
                name: String::from("Test user"),
                last_activity: None,
            },
        );

        let tag_uuid = uuid::Uuid::new_v4();
        db.definition.tags.insert(
            tag_uuid,
            super::entities::Tag {
                uuid: tag_uuid,
                name: String::from("Tag"),
                order: 0,
            },
        );
        let meta_uuid = uuid::Uuid::new_v4();
        db.definition.metas.insert(
            meta_uuid,
            super::entities::Meta {
                uuid: meta_uuid,
                name: String::from("Table"),
                ..Default::default()
            },
        );
        let product_uuid = uuid::Uuid::new_v4();
        db.definition.products.insert(
            product_uuid,
            super::entities::Product {
                uuid: product_uuid,
                name: String::from("Product"),
                meta_uuid: Some(meta_uuid),
                tags: vec![tag_uuid],
                ..Default::default()
            },
        );
        let room_uuid = uuid::Uuid::new_v4();
        db.definition.rooms.insert(
            room_uuid,
            super::entities::Room {
                uuid: room_uuid,
                name: String::from("Room"),
                ..Default::default()
            },
        );

        let uuid_table = uuid::Uuid::new_v4();
        db.tables.insert(
            uuid_table,
            super::entities::Table {
                uuid: uuid_table,
                room_uuid,
                name: String::from("Table"),
                ..Default::default()
            },
        );
        db
    }
}
