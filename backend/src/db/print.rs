use rust_decimal::Decimal;

pub trait PrintTrait {
    const ESC: u8 = 27;
    const GS: u8 = 29;

    fn cut() -> Vec<u8> {
        vec![Self::ESC, b'@', Self::GS, b'V', 48]
    }

    fn sep(char: &str, size: usize, n: usize) -> Vec<u8> {
        let mut str = char.repeat(size);
        str.push('\n');
        let str = str.repeat(n);
        str.as_bytes().to_vec()
    }

    fn image(path: &std::path::Path, size: usize) -> Vec<Vec<u8>> {
        vec![]
    }

    fn product(name: &str, quantity: u64, price: Decimal, size: usize) -> Vec<u8> {
        vec![]
    }

    fn print(&self, config: &crate::db::config::Server, size: usize) -> Vec<Vec<u8>>;
}
