use futures::{SinkExt, StreamExt};
use std::net::SocketAddrV4;
use tokio_serde::formats::*;
use tokio_util::codec::*;

use super::ContextWrapped;

#[derive(Clone, Debug)]
pub struct Device {
    pub uuid: Option<uuid::Uuid>,
    pub kind: Option<crate::db::config::DeviceType>,
    pub sender: tokio::sync::mpsc::Sender<super::message::Device>,
}

impl Device {
    pub async fn run(
        &mut self,
        context: super::ContextWrapped,
        mut stream: tokio_serde::Framed<
            Framed<tokio::net::TcpStream, LengthDelimitedCodec>,
            crate::server::message::Device,
            crate::server::message::Device,
            SymmetricalMessagePack<crate::server::message::Device>,
        >,
        mut receiver: tokio::sync::mpsc::Receiver<super::message::Device>,
    ) {
        let mut global_broadcast = context.read().await.global_broadcast.subscribe();
        tracing::debug!("Device connected");

        loop {
            tokio::select! {
                msg = global_broadcast.recv() => {
                    match msg {
                        Ok(super::message::Global::Reload) => {
                            tracing::info!("Reloading WebServer");
                        }
                        Ok(super::message::Global::Stop) => {
                            tracing::info!("Stopping WebServer");
                            break;
                        }
                        Err(err) => {
                            tracing::error!("{}", err);
                            break;
                        }
                    }
                }
                msg = stream.next() => {
                    match msg {
                      Some(Ok(crate::server::message::Device::Auth(token))) => {

                        let authorized = if let Some(device) = context.read().await.database.auth.devices.values().find(|d| d.token == token) {
                            self.uuid = Some(device.uuid.clone());
                            self.kind = Some(device.kind.clone());
                            true
                        } else {
                            false
                        };
                        if authorized {
                            tracing::debug!("Device is logged: {}", token);
                            context.write().await.device_set(self.clone());
                        }
                      }
                      Some(Ok(msg)) => {
                        if self.uuid.is_some() {
                            self.process(msg, context.clone()).await;
                        } else {
                            let _ = stream.close().await;
                            break;
                        }
                      }
                      Some(Err(err)) => {
                        tracing::error!("{}", err);
                        let _ = stream.close().await;
                        break;
                      }
                      None => {
                        let _ =stream.close().await;
                        break;
                      }
                    };
                }
                Some(msg) = receiver.recv() => {
                    stream.send(msg).await.unwrap();
                }
            }
        }
    }

    async fn process(&mut self, msg: crate::server::message::Device, context: ContextWrapped) {
        match msg {
            crate::server::message::Device::Weight(weight) => {
                context
                    .write()
                    .await
                    .client_broadcast(crate::server::message::Web::Weight(weight), false, false)
                    .await;
            }
            _ => {
                tracing::debug!("Unsupported message: {:?}", msg);
            }
        }
    }
}

pub struct Devices {
    pub context: super::ContextWrapped,
}
impl Devices {
    pub async fn run(&mut self, address: SocketAddrV4) {
        if let Ok(bind) = tokio::net::TcpListener::bind(address).await {
            let mut global_broadcast = self.context.read().await.global_broadcast.subscribe();

            loop {
                tokio::select! {
                    client = bind.accept() => {
                        if let Ok((socket,_)) = client {
                            let length_delimited = Framed::new(socket, LengthDelimitedCodec::new());
                            let stream = tokio_serde::SymmetricallyFramed::new(
                                length_delimited,
                                SymmetricalMessagePack::<crate::server::message::Device>::default(),
                            );

                            let (sender, receiver) =
                                tokio::sync::mpsc::channel::<crate::server::message::Device>(64);
                            let mut device = Device {
                                uuid: None,
                                kind: None,
                                sender,
                            };
                            let context = self.context.clone();

                            tokio::spawn(async move {
                                device.run(context, stream, receiver).await;
                            });
                        }
                    }

                    msg = global_broadcast.recv() => {
                        match msg {
                            Ok(super::message::Global::Reload) => {
                                tracing::info!("Reloading WebServer");
                            }
                            Ok(super::message::Global::Stop) => {
                                tracing::info!("Stopping WebServer");
                                break;
                            }
                            Err(err) => {
                                tracing::error!("{}", err);
                                break;
                            }
                        }
                    }

                }
            }
        }
    }
}
