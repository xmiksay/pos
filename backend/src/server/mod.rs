use std::collections::BTreeMap;

pub mod client;
pub mod device;
pub mod message;
pub mod web;

pub struct Context {
    pub database: crate::db::Database,
    // All workers listen this events
    pub global_broadcast: tokio::sync::broadcast::Sender<message::Global>,
    // All web clients listen this event
    pub web_broadcast: tokio::sync::broadcast::Sender<message::Web>,
    // All device client listen this event
    pub device_broadcast: tokio::sync::broadcast::Sender<message::Device>,
    // Device clients
    pub devices: BTreeMap<uuid::Uuid, device::Device>,
    // Web clients
    pub clients: BTreeMap<uuid::Uuid, client::Client>,
}

impl Context {
    pub fn client_set(&mut self, client: client::Client) {
        tracing::debug!("Register WS client: {}", client.uuid);
        self.clients.insert(client.uuid, client);
    }

    pub fn client_rm(&mut self, uuid: &uuid::Uuid) {
        tracing::debug!("DeRegister WS client: {}", uuid);
        self.clients.remove(uuid);
    }

    pub fn device_set(&mut self, device: device::Device) {
        if let Some(uuid) = &device.uuid {
            tracing::debug!("Register device: {}", uuid);
            self.devices.insert(uuid.clone(), device);
        }
    }

    pub fn device_rm(&mut self, uuid: &uuid::Uuid) {
        tracing::debug!("DeRegister device: {}", uuid);
        self.devices.remove(uuid);
    }

    /**
    Broadcast to all client if:

      is_locked:
      - true - client can be locked
      - false - client must not be locked

      is_admin:
      - true - client must be admin
      - false - client need not be admin
    */
    pub async fn client_broadcast(&self, msg: message::Web, is_locked: bool, is_admin: bool) {
        for client in self.clients.values().filter(|c| {
            (is_locked || c.is_locked == is_locked) && (!is_admin || is_admin == c.is_admin())
        }) {
            client.send(msg.clone()).await
        }
    }

    pub async fn print(&self, device: uuid::Uuid, data: Vec<Vec<u8>>) {
        if let Some(device) = self.devices.get(&device) {
            if let Err(err) = device.sender.send(message::Device::Pos(data)).await {
                tracing::error!("{}", err);
            }
        }
    }
}

pub type ContextWrapped = std::sync::Arc<tokio::sync::RwLock<Context>>;
pub struct Server {
    pub context: ContextWrapped,
}

impl Server {
    pub fn new(context: std::sync::Arc<tokio::sync::RwLock<Context>>) -> Self {
        Self { context }
    }

    pub async fn run(&mut self) -> anyhow::Result<()> {
        tracing::debug!("Starting server...");
        let mut global_receiver = {
            let context = self.context.write().await;
            context.global_broadcast.subscribe()
        };

        let mut web = web::Web::new(self.context.clone());
        let web_future = tokio::spawn(async move {
            let _ = web.run().await;
        });

        let mut devices = device::Devices {
            context: self.context.clone(),
        };
        let address = self
            .context
            .write()
            .await
            .database
            .config
            .base
            .port_tcp
            .clone();
        let device_future = tokio::spawn(async move {
            let _ = devices.run(address).await;
        });

        while let Ok(msg) = global_receiver.recv().await {
            match msg {
                message::Global::Reload => {
                    tracing::info!("Reloading server");
                }
                message::Global::Stop => {
                    tracing::info!("Stopping server");
                    break;
                }
            }
        }

        web_future.await?;
        device_future.await?;

        self.context.read().await.database.save()?;

        Ok(())
    }
}
