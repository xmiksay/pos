use std::collections::BTreeMap;

use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize, PartialEq)]
pub enum Global {
    Reload,
    Stop,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct Match<A> {
    pub key: uuid::Uuid,
    pub data: A,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct TimeSpan {
    pub date_from: Option<chrono::DateTime<chrono::Utc>>,
    pub date_to: Option<chrono::DateTime<chrono::Utc>>,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct UserInfo {
    pub uuid: uuid::Uuid,
    pub nick: String,
    pub name: String,
    pub is_locked: bool,
    pub is_admin: bool,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize, PartialEq)]
#[serde(rename_all = "camelCase", default)]
pub struct TokenInfo {
    pub value: String,
    pub expiration: chrono::DateTime<chrono::Utc>,
    pub locked: bool,
    pub nick: String,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize, PartialEq)]
#[serde(transparent)]

pub struct PrintInner {
    #[serde(with = "hex")]
    data: Vec<u8>,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct Print {
    printer: uuid::Uuid,
    data: PrintInner,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize, PartialEq)]
#[serde(rename_all = "camelCase", tag = "tag", content = "content")]
pub enum Web {
    /**
     * Inputs
     */
    // Authorize WS connection
    Login {
        nick: String,
        password: String,
    },
    Logout,
    // unlock screen
    Unlock(String),
    // Lock screen
    Lock,
    Token(String),
    // Get new token
    TokenGet(bool),

    PinSet {
        user_uuid: uuid::Uuid,
        pin: String,
    },
    PasswordSet {
        user_uuid: uuid::Uuid,
        password: String,
    },
    // set authorizations
    AuthSet(crate::db::Auth),
    // Set configuration
    ConfigSet(crate::db::config::Server),
    // Set definition
    DefinitionSet(crate::db::Definition),
    // Set all table
    TableSet(BTreeMap<uuid::Uuid, crate::db::entities::Table>),
    // synchronize table and items
    TableSync(crate::db::entities::Table),
    // Remove table
    TableRemove(uuid::Uuid),
    // synchronize invoice and items
    InvoiceSync(crate::db::entities::Invoice),
    InvoiceRemove(uuid::Uuid),
    // Sync source
    SourceSync(crate::db::entities::Source),
    SourceRemove(uuid::Uuid),
    // Query summary for time span
    Summary(Match<TimeSpan>),
    // Return report for time span
    Report(Match<TimeSpan>),
    PrintTable(uuid::Uuid),
    PrintInvoice(uuid::Uuid),
    Print(Print),

    /**
     * Outputs
     */
    // Authorization messages
    Auth(crate::db::Auth),
    // Get token
    TokenInfo(TokenInfo),
    // Token fail
    TokenFailed(String),
    // information about user
    UserInfo(Option<UserInfo>),
    // Broadcast server config/change
    Config(crate::db::config::Server),
    // Send only part of the config
    Setting(crate::db::config::Setting),
    // Broadcast
    Definition(crate::db::Definition),
    // Broadcast table/change
    Table(crate::db::entities::Table),
    // Return list of table after login
    Tables(crate::db::Tables),
    TableDeleted(uuid::Uuid),
    // Broadcast invoice/change
    Invoice(crate::db::entities::Invoice),
    // List of invoices
    Invoices(std::collections::BTreeMap<uuid::Uuid, crate::db::entities::Invoice>),
    InvoiceDeleted(uuid::Uuid),
    // Broudcast of source/change
    Source(crate::db::entities::Source),
    // List of sources
    Sources(std::collections::BTreeMap<uuid::Uuid, crate::db::entities::Source>),
    SourceDeleted(uuid::Uuid),
    // Information about connected device
    Device(crate::db::entities::Device),

    // Information about successfull paiment
    Paid(crate::db::entities::Invoice),

    // Send weight
    Weight(i64),

    Error(String),

    // Close connection request
    #[default]
    Close,
}

/*
#[derive(Clone, Debug, Default, Deserialize, Serialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct DeviceAuth {
    pub token: String,
    pub kind: crate::db::config::DeviceType,
}
*/

#[derive(Clone, Debug, Default, Deserialize, Serialize, PartialEq)]
#[serde(rename_all = "camelCase", tag = "tag", content = "content")]
pub enum Device {
    Auth(String),
    Ping(String),
    Pong(String),
    Weight(i64),
    Pos(Vec<Vec<u8>>),
    #[default]
    Nope,
}
