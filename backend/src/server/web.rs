use chrono::prelude::DateTime;
use futures_util::{SinkExt, StreamExt};
use serde::{Deserialize, Serialize};
use warp::{filters::path::path, Filter, Rejection};

use super::message;

type Result<T> = std::result::Result<T, Rejection>;

pub struct Web {
    pub context: std::sync::Arc<tokio::sync::RwLock<super::Context>>,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ReportParam {
    date_from: DateTime<chrono::Utc>,
    date_to: DateTime<chrono::Utc>,
}

impl Web {
    pub fn new(context: super::ContextWrapped) -> Self {
        Self { context }
    }

    pub fn with_context(
        context: super::ContextWrapped,
    ) -> impl Filter<Extract = (super::ContextWrapped,), Error = std::convert::Infallible> + Clone
    {
        warp::any().map(move || context.clone())
    }

    async fn websocket_handler(
        ws: warp::ws::Ws,
        context: super::ContextWrapped,
    ) -> Result<impl warp::Reply> {
        Ok(ws.on_upgrade(move |socket| Web::client_connection(socket, context)))
    }

    async fn report_full_handler(
        body: ReportParam,
        context: super::ContextWrapped,
    ) -> Result<impl warp::Reply> {
        tracing::debug!("Report handler");

        let report = crate::db::report::ReportFull::calculate(
            &context.read().await.database,
            &None,
            body.date_from,
            body.date_to,
        );
        Ok(serde_json::to_string(&report).unwrap())
    }

    async fn report_daily_handler(
        body: ReportParam,
        context: super::ContextWrapped,
    ) -> Result<impl warp::Reply> {
        tracing::debug!("Report handler");

        let report = crate::db::report::ReportDaily::calculate(
            &context.read().await.database,
            &None,
            body.date_from,
            body.date_to,
        );
        Ok(serde_json::to_string(&report).unwrap())
    }

    fn encode(msg: &message::Web) -> std::result::Result<warp::ws::Message, String> {
        if let Ok(value) = serde_json::to_string(msg) {
            Ok(warp::ws::Message::text(value))
        } else {
            Err(String::from("Unable encode message"))
        }
    }

    async fn client_connection(ws: warp::ws::WebSocket, context: super::ContextWrapped) {
        let uuid = uuid::Uuid::new_v4();

        let (mut ws_sender, mut ws_receiver) = ws.split();
        let (mut global_receiver, mut web_receiver, query_size) = {
            let context = context.read().await;
            (
                context.global_broadcast.subscribe(),
                context.web_broadcast.subscribe(),
                context.database.config.base.query_size,
            )
        };
        let (sender, mut receiver) = tokio::sync::mpsc::channel(query_size);
        let mut client = super::client::Client {
            uuid,
            context,
            sender,
            is_locked: true,
            user: None,
        };

        // Register client to context
        client.context.write().await.client_set(client.clone());
        client.send_user_info().await;

        loop {
            tokio::select! {
                // Global message processing
                msg = global_receiver.recv() => {
                    match msg {
                        Ok(message::Global::Reload) => {
                            tracing::info!("Reloading WebServer");
                        }
                        Ok(message::Global::Stop) => {
                            tracing::info!("Stopping WebServer");
                            break;
                        }
                        Err(err) => {
                            tracing::error!("{}", err);
                            break;
                        }
                    }
                }

                // Websocket message processing
                msg = ws_receiver.next() => {
                    if let Some(Ok(ws_msg)) = msg {
                        if ws_msg.is_text() {
                            let text_msg = ws_msg.to_str().unwrap();
                            match serde_json::from_str::<message::Web>(text_msg) {
                                Ok(message) => {
                                    if let Err(err) = client.process(message).await {
                                        tracing::error!("{}", err);
                                    } else {
                                        continue;
                                    }
                                }
                                Err(err) => {
                                    tracing::error!("{} {}", err, text_msg);
                                }
                            }
                        }
                    }
                    tracing::debug!("No other message");
                    break;
                }

                // Send local message to WS
                Some(msg) = receiver.recv() => {
                    if msg == message::Web::Close {
                        let _ = ws_sender.close().await;
                        break;
                    }
                    if let Ok(message_json) = Web::encode(&msg) {
                        if let Err(err) = ws_sender.send(message_json).await {
                            tracing::error!("Unable to send message: {} {:?}", err, msg);
                        }
                    }
                }

                // Send broadcast message to WS
                Ok(msg) = web_receiver.recv() => {
                    if msg == message::Web::Close {
                        let _ = ws_sender.close().await;
                        break;
                    }

                    if let Ok(message_json) = Web::encode(&msg) {
                        if let Err(err) = ws_sender.send(message_json).await {
                            tracing::error!("Unable to send message: {} {:?}", err, msg);
                        }
                    }
                }
            }
        }

        tracing::debug!("Client is deth");

        // Deregister client from context
        client.context.write().await.client_rm(&client.uuid);
    }

    pub fn websocket_route(
        context: super::ContextWrapped,
    ) -> impl Filter<Extract = (impl warp::Reply,), Error = warp::Rejection> + Clone {
        warp::path!("api" / "barman")
            .and(warp::ws())
            .and(Web::with_context(context))
            .and_then(Web::websocket_handler)
    }

    pub fn report_full_route(
        context: super::ContextWrapped,
    ) -> impl Filter<Extract = (impl warp::Reply,), Error = warp::Rejection> + Clone {
        warp::path!("api" / "report" / "full")
            .and(warp::body::json())
            .and(Web::with_context(context))
            .and_then(Web::report_full_handler)
    }

    pub fn report_daily_route(
        context: super::ContextWrapped,
    ) -> impl Filter<Extract = (impl warp::Reply,), Error = warp::Rejection> + Clone {
        warp::path!("api" / "report" / "daily")
            .and(warp::body::json())
            .and(Web::with_context(context))
            .and_then(Web::report_daily_handler)
    }

    fn routes(
        &self,
        config: crate::db::config::Server,
    ) -> impl Filter<Extract = (impl warp::Reply,), Error = warp::Rejection> + Clone {
        // WebSocket route
        Web::websocket_route(self.context.clone())
            .or(Web::report_full_route(self.context.clone()))
            .or(Web::report_daily_route(self.context.clone()))
            // Serve logo with image header
            .or(path("logo")
                .and(warp::fs::file(config.base.get_logo_path()))
                .map(|reply: warp::filters::fs::File| {
                    warp::reply::with_header(reply, "Content-Type", "image")
                }))
            // Serve web pages
            .or(warp::fs::dir(config.base.frontend_path.clone()))
            // Default serve index
            .or(warp::fs::file(format!(
                "{}/index.html",
                config.base.frontend_path
            )))
    }

    pub async fn run(&mut self) -> anyhow::Result<()> {
        let (mut global_receiver, config) = {
            let context = self.context.read().await;
            (
                context.global_broadcast.subscribe(),
                context.database.config.clone(),
            )
        };
        let socket = config.base.port_http;
        let routes = self.routes(config);
        let (_addr, fut) = warp::serve(routes).bind_with_graceful_shutdown(socket, async move {
            while let Ok(msg) = global_receiver.recv().await {
                match msg {
                    message::Global::Reload => {
                        tracing::info!("Reloading WebServer");
                    }
                    message::Global::Stop => {
                        tracing::info!("Stopping WebServer");
                        break;
                    }
                }
            }
        });
        fut.await;
        Ok(())
    }
}
