use uuid::timestamp::context;

use crate::{
    db::PrintTrait,
    server::message::{TokenInfo, Web},
};
use std::collections::BTreeMap;

#[derive(Clone)]
pub struct Client {
    pub uuid: uuid::Uuid,
    pub context: super::ContextWrapped,
    pub sender: tokio::sync::mpsc::Sender<super::message::Web>,
    pub user: Option<crate::db::entities::User>,
    pub is_locked: bool,
}

impl Client {
    pub fn is_admin(&self) -> bool {
        if let Some(user) = &self.user {
            user.is_admin
        } else {
            false
        }
    }
    /**
     * Get information about user
     */
    fn get_user_info(&self) -> Option<super::message::UserInfo> {
        self.user.as_ref().map(|user| super::message::UserInfo {
            uuid: user.uuid,
            nick: user.nick.clone(),
            name: user.name.clone(),
            is_locked: self.is_locked,
            is_admin: user.is_admin,
        })
    }
    pub async fn send(&self, msg: super::message::Web) {
        if let Err(err) = self.sender.send(msg).await {
            tracing::error!("{}", err);
        }
    }

    pub async fn send_user_info(&self) {
        self.send(Web::UserInfo(self.get_user_info())).await;
    }

    pub async fn send_definition(&self) {
        let context = self.context.read().await;
        self.send(Web::Definition(context.database.definition.clone()))
            .await
    }

    pub async fn send_tables(&self) {
        let context = self.context.read().await;
        self.send(Web::Tables(context.database.tables.clone()))
            .await
    }

    pub async fn send_invoices(&self) {
        let context = self.context.read().await;
        let invoices = if !self.is_admin() {
            context.database.invoices.map.clone()
        } else {
            context
                .database
                .invoice_filter(&self.user)
                .iter()
                .map(|&i| (i.uuid, i.clone()))
                .collect()
        };

        self.send(Web::Invoices(invoices)).await
    }

    pub async fn send_auth(&self) {
        let context = self.context.read().await;
        self.send(Web::Auth(context.database.auth.clone())).await;
    }

    pub async fn send_config(&self) {
        let context = self.context.read().await;
        self.send(Web::Config(context.database.config.clone()))
            .await
    }

    pub async fn send_sources(&self) {
        let context = self.context.read().await;
        self.send(Web::Sources(context.database.sources.map.clone()))
            .await
    }

    pub async fn send_setting(&self) {
        let context = self.context.read().await;
        self.send(Web::Setting(context.database.config.setting.clone()))
            .await
    }

    pub async fn refresh(&self) {
        self.send_user_info().await;
        if self.user.is_some() && !self.is_locked {
            self.send_tables().await;
            self.send_definition().await;
            self.send_invoices().await;

            if self.is_admin() {
                self.send_auth().await;
                self.send_config().await;
                self.send_sources().await;
            } else {
                self.send_setting().await;
            }
        }
    }
    /**
     * Process message from WS client
     */
    pub async fn process(&mut self, msg: super::message::Web) -> anyhow::Result<()> {
        if let Some(user) = &mut self.user {
            user.last_activity = Some(chrono::offset::Utc::now());
        };
        match msg {
            Web::Login { nick, password } => self.login(nick, password).await,
            Web::Lock => {
                self.is_locked = true;
                self.context.write().await.client_set(self.clone());
                self.send_user_info().await;
            }
            Web::Unlock(pin) => self.unlock(pin).await,
            Web::Logout => {
                self.user = None;
                self.is_locked = true;
                self.context.write().await.client_set(self.clone());
                self.send_user_info().await;
            }
            Web::Token(value) => self.token(value).await,
            Web::TokenGet(locked) => self.token_get(locked).await,
            Web::ConfigSet(config) => self.set_config(config).await,
            Web::DefinitionSet(definition) => self.definition_set(definition).await,
            Web::TableSet(tables) => self.table_set(tables).await,
            Web::TableSync(table) => self.table_sync(table).await,
            Web::TableRemove(uuid) => self.table_remove(uuid).await,
            Web::InvoiceSync(invoice) => self.invoice_sync(invoice).await,
            Web::SourceSync(source) => self.source_sync(source).await,
            Web::Close => {
                tracing::debug!("Client request close");
                self.send(msg).await;
            }
            Web::PrintInvoice(uuid) => {
                tracing::debug!("Print invoice: {}", uuid);
                let context = self.context.read().await;
                if let Some(invoice) = context.database.invoices.map.get(&uuid) {
                    if let Some(printer) = context.database.config.setting.printer_bill {
                        if let Some(device) = context.devices.get(&printer) {
                            let data = invoice.print(&context.database.config, 48);
                            if let Err(err) = device
                                .sender
                                .send(crate::server::message::Device::Pos(data))
                                .await
                            {
                                tracing::debug!("Problem with printing: {}", err);
                            }
                        }
                    }
                }
            }
            Web::PrintTable(uuid) => tracing::debug!("Print table: {}", uuid),
            Web::Print(print) => tracing::debug!("Custom print: {:?}", print),
            _ => {
                tracing::debug!("Unexpected messages, I am autist");
            }
        };
        Ok(())
    }

    /**
     * Login function for WS client
     */
    pub async fn login(&mut self, nick: String, password: String) {
        let logged = {
            let mut context = self.context.write().await;

            if let Some(user) = context
                .database
                .auth
                .users
                .values()
                .find(|u| u.nick.eq(&nick))
                .cloned()
            {
                if user.password_check(&password, &context.database.config.base.salt)
                    && user.is_enabled
                {
                    self.user = Some(user.clone());
                    self.is_locked = false;
                    context.client_set(self.clone());

                    tracing::debug!("User is logged {:?}", self.user);
                    true
                } else {
                    false
                }
            } else {
                false
            }
        };

        if logged {
            self.refresh().await;
        } else {
            self.send_user_info().await;
        }
    }

    pub async fn token(&mut self, value: String) {
        {
            let mut context = self.context.write().await;
            let now = chrono::offset::Utc::now();
            let logged = if let Some(token) = context
                .database
                .auth
                .tokens
                .values()
                .find(|t| t.value == value)
                .cloned()
            {
                tracing::debug!("{:?}", token);
                if token.expiration >= now {
                    if let Some(user) = context.database.auth.users.get_mut(&token.user_uuid) {
                        self.user = Some(user.clone());
                        self.is_locked = token.locked;
                        true
                    } else {
                        false
                    }
                } else {
                    false
                }
            } else {
                false
            };

            if !logged {
                self.user = None;
                self.is_locked = true;
                // Send that token failed
                self.send(Web::TokenFailed(value)).await;
            }
            context.client_set(self.clone());
        }

        tracing::debug!("Send user info");
        self.refresh().await;
    }

    pub async fn token_get(&mut self, locked: bool) {
        use blake2::{Blake2s256, Digest};
        if let Some(user) = &mut self.user {
            let mut context = self.context.write().await;
            let now = chrono::offset::Utc::now();
            let mut hasher = Blake2s256::new();
            hasher.update(now.timestamp_micros().to_be_bytes());
            hasher.update(&context.database.config.base.salt);

            let token = crate::db::entities::Token {
                expiration: now
                    + std::time::Duration::from_secs(
                        (context.database.config.setting.retention_days * 3600 * 24) as u64,
                    ),
                locked,
                value: hex::encode(hasher.finalize()),
                user_uuid: user.uuid,
            };
            context
                .database
                .auth
                .tokens
                .insert(token.value.clone(), token.clone());

            let token_info = TokenInfo {
                value: token.value,
                locked,
                expiration: token.expiration,
                nick: user.nick.clone(),
            };

            context.database.auth_save().unwrap();

            self.send(Web::TokenInfo(token_info)).await;
        }
    }

    /**
     * Check pin and unlock connection
     */
    pub async fn unlock(&mut self, pin: String) {
        if let Some(user) = &mut self.user {
            if user.pin == pin {
                self.is_locked = false;
                self.context.write().await.client_set(self.clone());
                self.refresh().await;
            } else {
                self.is_locked = true;
                self.send_user_info().await;
            }
        }
    }

    /**
     * Save server config
     * Only admin can
     */
    pub async fn set_config(&mut self, config: crate::db::config::Server) {
        if self.is_admin() && !self.is_locked {
            let mut context = self.context.write().await;
            context.database.config = config;
            if let Err(err) = context.database.config.save() {
                tracing::error!("{}", err);
            } else {
                context
                    .client_broadcast(Web::Config(context.database.config.clone()), false, true)
                    .await;
            }
        }
    }

    /* Send definition to client */
    pub async fn definition_set(&mut self, definition: crate::db::Definition) {
        if self.is_admin() {
            let mut context = self.context.write().await;
            if let Err(err) = context.database.definition_set(definition) {
                tracing::error!("{}", err);
            } else {
                // Broadcast to all active clients
                context
                    .client_broadcast(
                        Web::Definition(context.database.definition.clone()),
                        false,
                        false,
                    )
                    .await;
            }
        }
    }

    /* Send definition to client */
    pub async fn table_set(&mut self, tables: BTreeMap<uuid::Uuid, crate::db::entities::Table>) {
        if self.is_admin() {
            let mut context = self.context.write().await;
            if let Err(err) = context.database.tables_set(tables) {
                tracing::error!("{}", err);
            } else {
                // Broadcast to all active clients
                context
                    .client_broadcast(Web::Tables(context.database.tables.clone()), false, false)
                    .await;
            }
        }
    }

    pub async fn table_sync(&self, table: crate::db::entities::Table) {
        let uuid = table.uuid;
        let mut context = self.context.write().await;

        let _ = context.database.table_sync(table);

        if let Ok(table) = context.database.table_get(&uuid) {
            context
                .client_broadcast(Web::Table(table.clone()), false, false)
                .await;
        }
    }

    pub async fn table_remove(&self, uuid: uuid::Uuid) {
        {
            let mut context = self.context.write().await;

            if let Err(err) = context.database.table_remove(uuid) {
                tracing::error!("{}", err);
            }
        }

        self.send_tables().await
    }

    pub async fn invoice_sync(&self, invoice: crate::db::entities::Invoice) {
        let uuid = invoice.uuid;
        let mut context = self.context.write().await;

        if let Err(err) = context.database.invoice_sync(invoice) {
            tracing::error!("{}", err);
        }

        tracing::debug!("Syncing invoice {}", uuid);
        if let Ok(invoice) = context.database.invoice_get(&uuid) {
            context
                .client_broadcast(Web::Invoice(invoice.clone()), false, false)
                .await;
            tracing::debug!("Syncing DONE");
        }
    }

    pub async fn source_sync(&self, source: crate::db::entities::Source) {
        let uuid = source.uuid;
        let mut context: tokio::sync::RwLockWriteGuard<'_, super::Context> =
            self.context.write().await;

        if let Err(err) = context.database.source_sync(source.clone()) {
            tracing::error!("{}", err);
        }

        let source = context
            .database
            .source_get(&uuid)
            .unwrap_or(&source)
            .clone();

        context
            .client_broadcast(Web::Source(source), false, true)
            .await;
    }
}
